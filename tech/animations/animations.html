<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" sizes="any" type="image/svg+xml" href="/shared/images/favicon.svg">
    <link rel="stylesheet" type="text/css" href="/shared/shared.css">
    <title>Slider and button to control web animation</title>

  </head>
  <body>

    <article>
      <h1>Control<br/>
        <span class="huge-text">SVG Web Animation</span><br/>
        with<br/>
        <span class="huge-text">Slider and Button</span>
      </h1>

      <p id="link_back_home"><a href="/">go to mb.sb</a></p>

      <aside>
        <address>
          Author: Matthias Braun
        </address>
        First published on <time datetime="2021-12-01">Wednesday, 2021-12-01</time>
        <br/>
        Last modified on <time datetime="2023-08-04">Thursday, 2023-08-04</time>
      </aside>

      <hr>

      <p>Here's how to animate an SVG using the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Web_Animations_API">Web Animations API</a> and control the animation using a start/stop button as well as a slider.</p>

      <h2>SVG Animation Demo</h2>

      <p>
      Use the slider and the button to control the animation of the SVG below.
      </p>

      <div>
        <input id="slider" type="range" min="0" max="100" step="1" value="0">
        <br>
        <button id="startStopBtn">Run animation</button>
      </div>
      <br>
      <div id="statusInfo">Status information goes here once you start the animation</div>
      <svg id="ellipse" width="30em" fill="CadetBlue" viewBox="-50 -50 100 100">
        <ellipse rx="40%" ry="30%"/>
        <style>.centeredText {font: bold 10px sans-serif; fill: RoyalBlue; text-anchor: middle; dominant-baseline: middle}</style>
        <a href="https://mb.sb" target="_blank"><text class="centeredText">I'm a link</text></a>
      </svg>

      <h2>Code</h2>
      <h3>The SVG</h3>

      First, we define the SVG within our HTML code:

      <pre class="block-of-code"><xmp><svg id="ellipse" width="30em" fill="CadetBlue" viewBox="-50 -50 100 100">
  <ellipse rx="40%" ry="30%"/>
  <style>.centeredText {font: bold 10px sans-serif; fill: RoyalBlue; text-anchor: middle; dominant-baseline: middle}</style>
  <a href="https://mb.sb" target="_blank"><text class="centeredText">I'm a link</text></a>
</svg></xmp></pre>

      <p>
      The SVG contains two elements, an ellipse and a link. Since we want to rotate the ellipse around its center in this example, it's necessary to put the elements in the center of the SVG. One way to do this is by setting both the x-min and y-min parameters of the SVG <code>viewBox</code> to -50 which is half of the <code>viewBox</code>'s width and height, 100.
      <p>

      Have look at <a href="https://www.digitalocean.com/community/tutorials/svg-svg-viewbox">this</a> or <a href="https://webdesign.tutsplus.com/tutorials/svg-viewport-and-viewbox-for-beginners--cms-30844">this</a> tutorial to learn more about SVG's <code>viewBox</code>.

      <h3>Create the Animation</h3>

      After the DOM, including our SVG, has loaded, we'll use JavaScript to create an animation for the SVG. There are <a href="https://stackoverflow.com/questions/807878/how-to-make-javascript-execute-after-page-load">various ways</a> to make sure the JavaScript executes after the DOM is ready. I've simply put the <code>script</code> block after the body. Alternatively, you can use a script block in front of the body and defer its execution by declaring it <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules">a JavaScript module</a>:

      <pre class="block-of-code"><xmp><script type="module"></xmp></pre>

      Whichever way you choose, the next part creates an <a href="https://developer.mozilla.org/en-US/docs/Web/API/Animation">Animation object</a>. When we run the animation, it will rotate the SVG 360 degrees and change its fill color.

      <pre class="block-of-code"><xmp>const changeRotationAndColor = [ {
        transform: 'rotate(360deg)',
        fill: 'RebeccaPurple'
        },
    ];

const animationTiming = {
      duration: 3000,
      // When the animation is finished, don't reset the SVG to its initial state
      fill: "forwards",
      easing: 'ease-in-out'
    }

// Create the animation object but pause it immediately to prevent it from running on page load
const animation = document.getElementById('ellipse').animate(
      changeRotationAndColor,
      animationTiming
    );
animation.pause();</xmp></pre>


      <h3>Add code to the button and the slider</h3>
      <p>The rest of the JavaScript code is responsible for changing the current progress of the animation we just created using a slider and a button.</p>

      <p>The following JavaScript assumes an HTML <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/range">input element of type range</a> with ID "slider", a button "startStopBtn", and a div "statusInfo".</p>

      <p>Once the user has pressed the start button to run the animation, we use <a href="https://developer.mozilla.org/en-US/docs/Web/API/setInterval"><code>setInterval</code></a> to keep the slider in sync with the animation by polling the animation's progress repeatedly.</p>

    <pre class="block-of-code"><xmp>// Duration of the animation in milliseconds
const animationDuration = animation.effect.getComputedTiming().duration;

const slider = document.getElementById('slider');
// When reloading the page, we reset the slider value.
// Otherwise, it would keep its value before the reload
slider.value = slider.min;

const startStopBtn = document.getElementById('startStopBtn')

const statusInfo = document.getElementById('statusInfo')

// Identifies the function that moves the slider automatically after starting the animation
let sliderMoveIntervalId;

function moveSliderPeriodically() {
      sliderMoveIntervalId = setInterval(() => {
            setSliderProgressToAnimationProgress();
            statusInfo.innerHTML=`Animation running, moved slider to ${getAnimationProgress()}%`;
          }, 100);
    };

// Gets the animation progress in percent, as an integer
function getAnimationProgress() {
      return Math.trunc(animation.currentTime / animationDuration * 100);
    }

function setSliderProgressToAnimationProgress() {
      slider.value = getAnimationProgress() / 100 * slider.max;
    }

startStopBtn.addEventListener('click', (e) => {
      if(animation.playState === "running") {
            // Stop the automated progress of the slider when the user presses the stop button
            clearInterval(sliderMoveIntervalId);
            setSliderProgressToAnimationProgress();

            animation.pause();

            statusInfo.innerHTML = `Animation stopped at ${getAnimationProgress()}% with button`;
            startStopBtn.textContent = "Run animation";
          }
      else {
            moveSliderPeriodically();
            animation.play();
            startStopBtn.textContent = "Stop animation";
          }
    }
    );

// The user moved the slider → Update the animation
slider.addEventListener('input', (e) => {
      // Stop the automated progress of the slider if the animation's running
      if(animation.playState === "running") {
            clearInterval(sliderMoveIntervalId);
            startStopBtn.textContent = "Run animation";
          }

      // We pause the animation no matter the playState since if the animation is finished,
      // setting animation.currentTime would resume running the animation
      animation.pause();

      // Change the animation's percentage to the percentage of the slider
      const sliderPercentage = slider.value / slider.max * 100;
      animation.currentTime = animationDuration / 100 * sliderPercentage;
      statusInfo.innerHTML = `User moved slider, set animation to ${getAnimationProgress()}%`;
    });

animation.onfinish = () => {
      statusInfo.innerHTML= "Animation finished";
      clearInterval(sliderMoveIntervalId);
      // Set the slider in case the last interval hasn't run
      setSliderProgressToAnimationProgress();
      startStopBtn.textContent = "Rerun animation";
    }</xmp></pre>


      <h2>Try it yourself</h2>
      To inspect and play around with the full SVG and animation code on this page, press <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>c</kbd>. This will open <a href="https://developer.mozilla.org/en-US/docs/Tools/Page_Inspector/How_to/Open_the_Inspector">the Inspector</a> in Chrome or Firefox.

      <hr class="last-horizontal-rule">
      <footer>

        Found something to improve? You can create a pull request for this article <a href="https://gitlab.com/bullbytes/personal_website">on GitLab</a>.

        <p>License: <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a></p>
      </footer>

    </article>
  </body>

  <!-- The script block accesses elements from the DOM, hence it has to go after the body. Alternatively, use 'script type="module"' to put the script block in front of the body -->
  <script>
    const changeRotationAndColor = [
          { transform: 'rotate(360deg)', fill: 'RebeccaPurple' },
        ];

    const animationTiming = {
          duration: 3000,
          fill: "forwards",
          easing: 'ease-in-out'
        }

    // Create the animation object but pause it immediately to prevent it from running on page load
    const animation = document.getElementById('ellipse').animate(
          changeRotationAndColor,
          animationTiming
        );
    animation.pause();

    // Duration of the animation in milliseconds
    const animationDuration = animation.effect.getComputedTiming().duration;

    const slider = document.getElementById('slider');
    // When reloading the page, we reset the slider value.
    // Otherwise, it would keep its value before the reload
    slider.value = slider.min;

    const startStopBtn = document.getElementById('startStopBtn')

    const statusInfo = document.getElementById('statusInfo')

    // Identifies the function that moves the slider automatically after starting the animation
    let sliderMoveIntervalId;

    function moveSliderPeriodically() {
        sliderMoveIntervalId = setInterval(() => {
                setSliderProgressToAnimationProgress();
                statusInfo.innerHTML=`Animation running, moved slider to ${getAnimationProgress()}%`;
              }, 100);
        };

    function setSliderProgressToAnimationProgress() {
          slider.value = getAnimationProgress() / 100 * slider.max;
        }

    // Gets the animation progress in percent, as an integer
    function getAnimationProgress() {
          return Math.trunc(animation.currentTime / animationDuration * 100);
        }

    startStopBtn.addEventListener('click', (e) => {
          if(animation.playState === "running") {
                // Stop the automated progress of the slider when the user presses the stop button
                clearInterval(sliderMoveIntervalId);
                setSliderProgressToAnimationProgress();

                animation.pause();

                statusInfo.innerHTML = `Animation stopped at ${getAnimationProgress()}% with button`;
                startStopBtn.textContent = "Run animation";
              }
          else {
                moveSliderPeriodically();
                animation.play();
                startStopBtn.textContent = "Stop animation";
              }
        }
        );

    // The user moved the slider → Update the animation
    slider.addEventListener('input', (e) => {
          // Stop the automated progress of the slider if the animation's running
          if(animation.playState === "running") {
                clearInterval(sliderMoveIntervalId);
                startStopBtn.textContent = "Run animation";
              }

          // We pause the animation no matter the playState since if the animation is finished,
          // setting animation.currentTime would resume running the animation
          animation.pause();

          // Change the animation's percentage to the percentage of the slider
          const sliderPercentage = slider.value / slider.max * 100;
          animation.currentTime = animationDuration / 100 * sliderPercentage;
          statusInfo.innerHTML = `User moved slider, set animation to ${getAnimationProgress()}%`;
        });

    animation.onfinish = () => {
          statusInfo.innerHTML= "Animation finished";
          clearInterval(sliderMoveIntervalId);
          // Set the slider in case the last interval hasn't run
          setSliderProgressToAnimationProgress();
          startStopBtn.textContent = "Rerun animation";
        }
  </script>
</html>
