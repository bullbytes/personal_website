// Imported function is defined in src/lib.rs
// wasm_bindgen was imported in index.html
const {set_up_worker} = wasm_bindgen;

async function run_wasm() {
    // Load the Wasm file to allow calling Rust functions
    await wasm_bindgen('./pkg/ana_carve_bg.wasm');

    console.log("index.js has loaded Wasm file → Functions defined with Rust are now available");

    set_up_worker();

    // Create a worker in JS. The worker also uses Rust functions
    // var myWorker = new Worker('./worker.js');
    // document.getElementById("file_picker").addEventListener(
    //     "change",
    //     function() {
    //
    //         console.log("eventListener 'change' inside index.js runs");
    //         let file = this.files[0];
    //
    //         var startTime = performance.now()
    //
    //         myWorker.postMessage({ file: file });
    //         myWorker.onmessage = function(e) {
    //             var endTime = performance.now()
    //             console.log(`Call to analyze_file took ${endTime - startTime} milliseconds`)
    //             console.log("Back in index.js, result of analysis is" , e.data);
    //         };
    //     },
    //     false
    // );
}

run_wasm();
