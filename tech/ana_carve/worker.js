importScripts('./pkg/ana_carve.js');

const {analyze_file} = wasm_bindgen;

async function run_in_worker() {
    // Load the Wasm file by awaiting the Promise returned by `wasm_bindgen`
    await wasm_bindgen('./pkg/ana_carve_bg.wasm');
    console.log("worker.js has loaded Wasm file → Functions defined with Rust are now available");
}

run_in_worker();


// We can send the worker a file using `postMessage` in JS or `post_message` in Rust.
// Inside a worker we can access the File API (https://developer.mozilla.org/en-US/docs/Web/API/File)
// but we can't access the web page's document (no updating of the GUI).
onmessage = async function(e) {
    console.log("onmessage inside worker.js runs");
    const file = e.data;
    let workerResult = analyze_file(file);
    // To define which function the worker calls using postMessage,
    // use `worker.set_onmessage` from Rust or `worker.onmessage` from JS.
    // Inside this callback function, the document is available again so we can update the GUI with
    // the worker's results.
    postMessage(workerResult);
};
