<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" sizes="any" type="image/svg+xml" href="/shared/images/favicon.svg">
    <link rel="stylesheet" type="text/css" href="/shared/shared.css">
    <link rel="stylesheet" type="text/css" href="shared.css">
    <title>Exercise: Decimal to Binary</title>
    <style>
      .binary-column {
        text-align: right;
      }
    </style>
  </head>
  <body>
    <h1>Exercise: Decimal to Binary</h1>

    <p>Write a console program that lets the user enter a positive decimal number. Convert that number to its binary form and print it.</p>

    <p>
    Example program output (user input in <span class='user-input'>green</span>):
<pre class='block-of-code'>
Enter a decimal number:
<span class='user-input'>13</span>
13 in binary is 1101.
</pre>

<pre class='block-of-code'>
Enter a decimal number:
<span class='user-input'>0</span>
0 in binary is 0.
</pre>

<pre class='block-of-code'>
Enter a decimal number:
<span class='user-input'>254</span>
254 in binary is 11111110.
</pre>


    </p>

    <h2>How to convert decimal to binary</h2>

    The steps to convert a decimal number to binary are as follows:
    <ol>
      <li>Start with the decimal number.</li>
      <li>Get the remainder of the number when divided by 2.</li>
      <li>Divide the decimal number by 2.</li>
      <li>Repeat steps 2 and 3 until the decimal number is 0.</li>
    </ol>

    The binary number is the sequence of remainders from step 2, read from the last remainder to the first remainder.


    <p>For example, to convert the decimal number 13 to binary:</p>
    <table>
      <tbody>
        <tr>
          <th>Decimal number</th>
          <th>Quotient</th>
          <th>Remainder</th>
        </tr>
        <tr>
          <td>13</td>
          <td>6</td>
          <td>1</td>
        </tr>
        <tr>
          <td>6</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <td>3</td>
          <td>1</td>
          <td>1</td>
        </tr>
        <tr>
          <td>1</td>
          <td>0</td>
          <td>1</td>
        </tr>
      </tbody>
    </table>

    Therefore, the binary representation of 13 is <code>1101</code>.

    <h3>Why does it work?</h3>
    <p>Why does repeatedly dividing the number by 2 and saving the remainder give us the binary representation of that number?</p>

    <p>
    One way to look at this is that the remainder of the division by 2 (in Java: <code>%2</code>) is the rightmost bit of the binary representation. The rightmost bit is also called the <em>least significant bit</em>. It's <code>0</code> for even numbers and <code>1</code> for odd numbers.
    </p>

    <p>
    The quotient of the division by 2 (in Java: <code>/2</code>) is the number that remains when you remove the least significant bit by shifting the bit sequence one bit to the right.
    </p>

    <p>
    If you repeat this process with the quotient, you get the next bit of the binary representation and so on.
    </p>

    Consider the binary representation of the decimal number 116, its least significant bit, and how those change after each division by 2:

<table border="1">
  <thead>
    <tr>
      <th>Decimal number</th>
      <th>Number in binary</th>
      <th>Least significant bit</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>116</td>
      <td class="binary-column">1110100</td>
      <td>0</td>
    </tr>
    <tr>
      <td>58</td>
      <td class="binary-column">111010</td>
      <td>0</td>
    </tr>
    <tr>
      <td>29</td>
      <td class="binary-column">11101</td>
      <td>1</td>
    </tr>
    <tr>
      <td>14</td>
      <td class="binary-column">1110</td>
      <td>0</td>
    </tr>
    <tr>
      <td>7</td>
      <td class="binary-column">111</td>
      <td>1</td>
    </tr>
    <tr>
      <td>3</td>
      <td class="binary-column">11</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td class="binary-column">1</td>
      <td>1</td>
    </tr>
  </tbody>
</table>

<p>
Going from bottom to top, you can see the binary representation of 116: <code>1110100</code>
</p>

In short, this method works because <code>%2</code> gets the rightmost bit, and <code>/2</code> shifts the bits to the right.


<h3>Verifying the conversion</h3>
    Once we have converted from decimal to binary, we can convert from <a href="./binary_to_decimal.html">binary to decimal</a> again, to make sure the conversion is correct:

    <table>
      <tbody>
        <tr>
          <th>bits:</th>
          <td>1</td>
          <td>1</td>
          <td>1</td>
          <td>0</td>
          <td>1</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>bit values:</th>
          <td>2<sup>6</sup></td>
          <td>2<sup>5</sup></td>
          <td>2<sup>4</sup></td>
          <td>2<sup>3</sup></td>
          <td>2<sup>2</sup></td>
          <td>2<sup>1</sup></td>
          <td>2<sup>0</sup></td>
        </tr>
      </tbody>
    </table>

    Going from right to left, we calculate the decimal value of <code>1110100</code>:

    <p>
      <math xmlns="http://www.w3.org/1998/Math/MathML" display="block">
        <mrow>
          <msup>
            <mn>2</mn>
            <mn>2</mn>
          </msup>
          <mo>+</mo>
          <msup>
            <mn>2</mn>
            <mn>4</mn>
          </msup>
          <mo>+</mo>
          <msup>
            <mn>2</mn>
            <mn>5</mn>
          </msup>
          <mo>+</mo>
          <msup>
            <mn>2</mn>
            <mn>6</mn>
          </msup>
          <mo>=</mo>
        </mrow>
        <mspace linebreak="newline" />
        <mrow>
          <mn>4</mn>
          <mo>+</mo>
          <mn>16</mn>
          <mo>+</mo>
          <mn>32</mn>
          <mo>+</mo>
          <mn>64</mn>
          <mo>=</mo>
          <mn>116</mn>
        </mrow>
      </math>
    </p>

    <h2>Hand in instructions</h2>
    <ol>
      <li>Make sure your program runs correctly.</li>
      <li>Hand in your program by uploading <b>Main.java</b> to Moodle.</li>
    </ol>

  </body>
</html>
