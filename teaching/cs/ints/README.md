# Integer Explorer
An interactive visualization for binary representations of integers. Work in progress.

## Build instructions

    # Just once
    cargo install wasm-pack # or pacman -S wasm-pack
    # Build the application, including WebAssembly and JavaScript files
    wasm-pack build --target web

## Run instructions
Start a web server to serve the files `integers.html`, `pkg/integer_explorer.js`, and `pkg/integer_explorer_bg.wasm`:

    # Start the web server, listening on localhost:8000
    python -m http.server
