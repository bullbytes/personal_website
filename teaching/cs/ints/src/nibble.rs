use std::fmt::{Display, Formatter};
use crate::bit::Bit;

#[derive(Debug, PartialEq, Clone)]
pub(crate) struct Nibble {
    index_3: Bit,
    index_2: Bit,
    index_1: Bit,
    index_0: Bit,
}

impl Nibble {

    pub(crate) fn from_little_endian_bits(bits: &[Bit]) -> Nibble {
        Nibble {
            index_3: bits[3],
            index_2: bits[2],
            index_1: bits[1],
            index_0: bits[0],
        }
    }
    pub(crate) fn from_little_endian_u8s(u8s: &[u8]) -> Nibble {
        Nibble {
            index_3: u8s[3].into(),
            index_2: u8s[2].into(),
            index_1: u8s[1].into(),
            index_0: u8s[0].into(),
        }
    }

    pub(crate) fn bits_big_endian(&self) -> Vec<Bit> {
        vec![self.index_3, self.index_2, self.index_1, self.index_0]
    }


    pub(crate) fn bits_little_endian(&self) -> Vec<Bit> {
        self.bits_big_endian().into_iter().rev().collect()
    }

    pub(crate) fn bits_little_endian_as_u8(&self) -> Vec<u8> {
        self.bits_little_endian().iter().map(|bit| (*bit).into()).collect()
    }

    pub(crate) fn flip(&self) -> Nibble {
        Nibble {
            index_3: self.index_3.flip(),
            index_2: self.index_2.flip(),
            index_1: self.index_1.flip(),
            index_0: self.index_0.flip(),
        }
    }
    pub(crate) fn flip_most_significant_bit(self) -> Nibble {
        Nibble {
            index_3: self.index_3.flip(),
            index_2: self.index_2,
            index_1: self.index_1,
            index_0: self.index_0,
        }
    }

    // Add one to the nibble. If the nibble is 1111, it wraps around to 0000.
    pub(crate) fn add_one(self) -> Nibble {
        let mut bits_le = self.bits_little_endian();
        for bit in &mut bits_le {
            if *bit == Bit::Zero {
                *bit = Bit::One;
                // We've added one and there's no carry, so we can break out of the loop
                break;
            }
            // Else, the bit was one. We set it to zero and continue adding one to the next bit
            *bit = Bit::Zero;
        }
        Nibble::from_little_endian_bits(&bits_le)
    }

    pub(crate) fn is_zero(&self) -> bool {
        self.index_3 == Bit::Zero && self.index_2 == Bit::Zero && self.index_1 == Bit::Zero && self.index_0 == Bit::Zero
    }

    /// Converts the nibble to a string representing the decimal number.
    /// The `sign_strategy` determines how the number is interpreted (e.g. as an unsigned number,
    /// with a sign bit, ones' complement, two's complement).
    /// Since sign bit and ones' complement have a negative zero, the return value is a string and
    /// not an integer.
    pub(crate) fn to_decimal(&self, sign_strategy: SignStrategy) -> String {
        match sign_strategy {
            SignStrategy::Unsigned => {

             self.bits_little_endian_as_u8().iter().enumerate()
                .fold(0u8, |cur_sum, (index, bit)| {
                    let cur_bit_value_in_decimal = 2u8.pow(index as u32) * bit;
                    cur_sum + cur_bit_value_in_decimal
                }).to_string()
            }
            SignStrategy::SignBit => {

            // The bit with the smallest value comes first
            let bits_as_u8 = self.bits_little_endian_as_u8();
            // The sign bit is at the position of the most significant bit
            let sign_bit = bits_as_u8[3];
            let decimal_value_of_bits = bits_as_u8.iter().take(3).enumerate()
                .fold(0u8, |cur_sum, (index, bit)| {
                    let cur_bit_value_in_decimal = 2u8.pow(index as u32) * bit;
                    cur_sum + cur_bit_value_in_decimal
                });
                // We prepend the negative sign as a string since we also want to have negative zero, which
                // we wouldn't have if we just multiplied the decimal value of the bits with -1.
                let signed_decimal: String = if sign_bit == 1 {
                    "-".to_owned() + &decimal_value_of_bits.to_string()
                } else {
                    decimal_value_of_bits.to_string()
                };
                signed_decimal
            }
            SignStrategy::OnesComplement => {
            // The bit with the smallest value comes first
            let bits_as_u8 = self.bits_little_endian_as_u8();
            // The sign bit is at the position of the most significant bit
            let sign_bit = bits_as_u8[3];
            let remaining_bits: Vec<u8> = if sign_bit == 1 {
                // Flip the bits if the number is negative. We'll add a minus sign later.
                bits_as_u8.iter().take(3).map(|bit| {
                    if *bit == 0 {
                        1
                    } else {
                        0
                    }
                }).collect()
            } else {
                bits_as_u8.iter().take(3).copied().collect()
            };
            let decimal_value_of_bits = remaining_bits.iter().enumerate()
                .fold(0u8, |cur_sum, (index, bit)| {
                    let cur_bit_value_in_decimal = 2u8.pow(index as u32) * bit;
                    cur_sum + cur_bit_value_in_decimal
                });
                // We prepend the negative sign as a string since we also want to have negative zero, which
                // we wouldn't have if we just multiplied the decimal value of the bits with -1.
                let signed_decimal: String = if sign_bit == 1 {
                    "-".to_owned() + &decimal_value_of_bits.to_string()
                } else {
                    decimal_value_of_bits.to_string()
                };
                signed_decimal
            }
            SignStrategy::TwosComplement => {
                let bits : Vec<i8> = self.bits_big_endian().iter().map(|bit| (*bit).into()).collect();
                let int: i8 = (-8 * bits[0]) + 4 * bits[1] + 2 * bits[2] + bits[3];
                int.to_string()
            }
        }
    }
}
impl Display for Nibble {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let index_3 = self.index_3.to_string();
        let index_2 = self.index_2.to_string();
        let index_1 = self.index_1.to_string();
        let index_0 = self.index_0.to_string();
        write!(f, "[{index_3}, {index_2}, {index_1}, {index_0}]")
    }
}

impl From<u32> for Nibble {
    fn from(int: u32) -> Self {
        let index_0 = int & 1;
        let index_0_bit = Bit::from(index_0);
        let mut changed_int = int >> 1;
        let index_1 = changed_int & 1;
        let index_1_bit = Bit::from(index_1);
        changed_int >>= 1;
        let index_2 = changed_int & 1;
        let index_2_bit = Bit::from(index_2);
        changed_int >>= 1;
        let index_3 = changed_int & 1;
        let index_3_bit = Bit::from(index_3);

        Nibble {
            index_3: index_3_bit,
            index_2: index_2_bit,
            index_1: index_1_bit,
            index_0: index_0_bit,
        }
    }
}

pub enum SignStrategy {
    Unsigned,
    SignBit,
    OnesComplement,
    TwosComplement,
}

#[test]
fn test_get_nibble_from_u32() {
    fn big_endian_bits_to_u8(nibble: Nibble) -> Vec<u8> {
        nibble.bits_big_endian().iter().map(|bit| (*bit).into()).collect()
    }

    assert_eq!(big_endian_bits_to_u8(Nibble::from(0)),  vec![0, 0, 0, 0]);
    assert_eq!(big_endian_bits_to_u8(Nibble::from(1)),  vec![0, 0, 0, 1]);
    assert_eq!(big_endian_bits_to_u8(Nibble::from(7)),  vec![0, 1, 1, 1]);
    assert_eq!(big_endian_bits_to_u8(Nibble::from(8)),  vec![1, 0, 0, 0]);
    assert_eq!(big_endian_bits_to_u8(Nibble::from(15)), vec![1, 1, 1, 1]);
}

#[test]
fn test_add_one_to_nibble() {
    assert_eq!(Nibble::from(0).add_one(), Nibble::from(1));
    assert_eq!(Nibble::from(1).add_one(), Nibble::from(2));
    assert_eq!(Nibble::from(2).add_one(), Nibble::from(3));
    assert_eq!(Nibble::from(4).add_one(), Nibble::from(5));
    // We wrap around to zero
    assert_eq!(Nibble::from(15).add_one(), Nibble::from(0));
}
