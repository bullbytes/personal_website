use std::fmt::{Display, Formatter};

#[derive(Debug, Clone, Copy)]
pub enum Css {
    DecimalResult,
    SignBit,
    BitsWithResult,
    EqualSign,
    BitValuesInDecimal,
}

impl Css {
    pub(crate) fn as_string(&self) -> &str {
        match self {
            Css::DecimalResult => "decimalResult",
            Css::SignBit => "signBit",
            Css::BitsWithResult => "bitsWithResult",
            Css::EqualSign => "equalSign",
            Css::BitValuesInDecimal => "bitValuesInDecimal",
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum ElementId {
    BitsOfUnsignedInteger,
    DecimalResultOfUnsignedInteger,
    UnsignedIntegersTableWithAllValues,
    BitsOfIntegerWithSignBit,
    DecimalResultOfIntegerWithSignBit,
    SignedIntegersWithSignBitTableWithAllValues,
    BitsOfIntegerWithOnesComplement,
    DecimalResultOfIntegerWithOnesComplement,
    SignedIntegersWithOnesComplementTableWithAllValues,
    BitsOfNumberToOnesComplement,
    CommentDecimalToNegateWithOnesComplement,
    CommentOnesComplementAsDecimal,
    BitsOfIntegerWithTwosComplement,
    DecimalResultOfIntegerWithTwosComplement,
    SignedIntegersWithTwosComplementTableWithAllValues,
    BitsOfNumberToTwosComplement,
    CommentDecimalToNegateWithTwosComplement,
    CommentTwosComplementAsDecimal,
}

impl ElementId {
    fn as_string(&self) -> &str {
        match self {
            ElementId::BitsOfUnsignedInteger => "bitsOfUnsignedInteger",
            ElementId::DecimalResultOfUnsignedInteger => "decimalResultOfUnsignedInteger",
            ElementId::UnsignedIntegersTableWithAllValues => "unsignedIntegersTableWithAllValues",
            ElementId::BitsOfIntegerWithSignBit=> "bitsOfIntegerWithSignBit",
            ElementId::DecimalResultOfIntegerWithSignBit => "decimalResultOfIntegerWithSignBit",
            ElementId::SignedIntegersWithSignBitTableWithAllValues =>"signedIntegersWithSignBitTableWithAllValues",
            ElementId::BitsOfIntegerWithOnesComplement => "bitsOfIntegerWithOnesComplement",
            ElementId::DecimalResultOfIntegerWithOnesComplement  => "decimalResultOfIntegerWithOnesComplement",
            ElementId::SignedIntegersWithOnesComplementTableWithAllValues  => "signedIntegersWithOnesComplementTableWithAllValues",
            ElementId::BitsOfNumberToOnesComplement => "bitsOfNumberToOnesComplement",
            ElementId::CommentDecimalToNegateWithOnesComplement => "commentDecimalToNegateWithOnesComplement",
            ElementId::CommentOnesComplementAsDecimal => "commentOnesComplementAsDecimal",
            ElementId::BitsOfIntegerWithTwosComplement => "bitsOfIntegerWithTwosComplement",
            ElementId::DecimalResultOfIntegerWithTwosComplement => "decimalResultOfIntegerWithTwosComplement",
            ElementId::SignedIntegersWithTwosComplementTableWithAllValues =>"signedIntegersWithTwosComplementTableWithAllValues",
            ElementId::BitsOfNumberToTwosComplement => "bitsOfNumberToTwosComplement",
            ElementId::CommentDecimalToNegateWithTwosComplement => "commentDecimalToNegateWithTwosComplement",
            ElementId::CommentTwosComplementAsDecimal => "commentTwosComplementAsDecimal",
        }
    }
}

impl Display for ElementId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.as_string())
    }
}
