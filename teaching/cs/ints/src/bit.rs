use std::fmt::{Display, Formatter};
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Bit {
    Zero,
    One,
}

impl From<u8> for Bit {
    fn from(u: u8) -> Self {
        match u {
            0 => Bit::Zero,
            1 => Bit::One,
            _ => panic!("u must be 0 or 1"),
        }
    }
}
impl From<u32> for Bit {
    fn from(int: u32) -> Self {
        match int {
            0 => Bit::Zero,
            1 => Bit::One,
            _ => panic!("u must be 0 or 1"),
        }
    }
}

impl From<Bit> for u8 {
    fn from(bit: Bit) -> Self {
        match bit {
            Bit::Zero => 0,
            Bit::One => 1,
        }
    }
}
impl From<Bit> for i8 {
    fn from(bit: Bit) -> Self {
        match bit {
            Bit::Zero => 0,
            Bit::One => 1,
        }
    }
}

impl Display for Bit {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let bit_str = match self {
            Bit::Zero => "0",
            Bit::One => "1",
        };
        write!(f, "{bit_str}")
    }
}

impl Bit {
    /// Converts an array of u8 to an array of Bits.
    /// For example, [1, 0, 1, 0, 1, 0, 1, 0] becomes `[Bit::One, Bit::Zero, Bit::One, Bit::Zero, Bit::One, Bit::Zero, Bit::One, Bit::Zero]`
    pub fn from_array_of_u8(arr: [u8; 8]) -> [Bit; 8] {
        let mut bits = [Bit::Zero; 8];
        for i in 0..=7 {
            bits[i] = arr[i].into();
        }
        bits
    }
    /// Gets the bits of a byte as an array, with the most significant bit at index 0 and the least significant bit at index 7.
    /// For example, if the byte is 130, the array will be `[1, 0, 0, 0, 0, 0, 1, 0]`
    pub fn get_bits_of_byte(byte: u8) -> [Bit; 8] {
        let mut bits = [Bit::Zero; 8];
        for i in 0..=7 {
            let shifted_byte = byte >> i;
            // Get the rightmost bit of the shifted byte (least significant bit)
            let cur_bit_as_u8 = shifted_byte & 1;
            // For the first iteration, the cur_bit is the least significant bit and therefore we place
            // that bit at index 7 of the array (rightmost bit)
            bits[7 - i] = if cur_bit_as_u8 == 0 {
                Bit::Zero
            } else {
                Bit::One
            };
        }
        bits
    }

    pub (crate) fn flip(self) -> Bit {
        match self {
            Bit::Zero => Bit::One,
            Bit::One => Bit::Zero,
        }
    }

}

#[test]
fn test_get_bits_of_byte() {
    let byte = 0b1010_1010;
    let bits = Bit::get_bits_of_byte(byte);
    assert_eq!(bits, Bit::from_array_of_u8([1, 0, 1, 0, 1, 0, 1, 0]));

    let byte = 255;
    let bits = Bit::get_bits_of_byte(byte);
    assert_eq!(bits, Bit::from_array_of_u8([1, 1, 1, 1, 1, 1, 1, 1]));

    let byte = 3;
    let bits = Bit::get_bits_of_byte(byte);
    assert_eq!(bits, Bit::from_array_of_u8([0, 0, 0, 0, 0, 0, 1, 1]));

    let byte = 130;
    let bits = Bit::get_bits_of_byte(byte);
    assert_eq!(bits, Bit::from_array_of_u8([1, 0, 0, 0, 0, 0, 1, 0]));
}
