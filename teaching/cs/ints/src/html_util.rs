use std::collections::HashMap;

use web_sys::{console, js_sys::Array, window, Document, Element, Event, HtmlElement};

use wasm_bindgen::{closure::IntoWasmClosure, prelude::Closure, JsCast, JsValue};

use crate::page_access::{ElementId,Css};

/// Adds a `class` to the `element` if it doesn't already have that `class`.
/// Returns the `element`.
pub fn add_class(class: Css, element: &Element) -> &Element {
    let class_as_str = class.as_string();
    let res = element.class_list().add_1(class_as_str);
    match res {
        Ok(()) => {}
        Err(e) => {
            let elem_id = element.id();
            log_to_browser(format!(
                "Could not add class with name '{class_as_str}' to element '{elem_id}'. Error: {e:?}"
            ));
        }
    };
    element
}

/// Creates a new table data element.
pub fn new_td(text_content: &str, attributes: HashMap<&str, &str>) -> Element {
    let table_data = get_document().create_element("td").unwrap();
    table_data.set_text_content(Some(text_content));
    for (key, value) in attributes {
        let _ = table_data.set_attribute(key, value);
    }
    table_data
}
pub fn new_simple_td<T: ToString>(content: T) -> Element {
    new_td(&content.to_string(), HashMap::new())
}

pub(crate) fn new_table_row_with_data<T: ToString>(data: &[T]) -> Result<Element, JsValue> {
    let row = get_document().create_element("tr")?;
    let table_data = data.iter().map(|x|
        new_simple_td(x.to_string()))
        .collect::<Array>();
    row.append_with_node(&table_data)?;
    Ok(row)
}


/// Logs a string to the browser's console.
pub fn log_to_browser(log_msg: String) {
    console::log_1(&log_msg.into());
}

/// Gets the first element that matches the `selector` inside the `containing_elem`.
pub fn query_selector(parent_elem: &Element, selector: &str) -> Option<Element> {
    match parent_elem.query_selector(selector) {
        Ok(found_elem_maybe) => found_elem_maybe,
        Err(err) => {
            let elem_id = parent_elem.id();
            log_to_browser(format!(
                "Couldn't get element with selector '{selector}'. Search started at element with ID '{elem_id}'. Error is: {err:?}"
            ));
            None
        }
    }
}

/// Gets an [[`Element`]] by its `elem_id`.
/// Warns if the element doesn't exist.
pub fn get_elem(elem_id: ElementId) -> Option<Element> {
    let elem_maybe = get_document().get_element_by_id(&elem_id.to_string());
    if elem_maybe.is_none() {
        log_to_browser(format!("Couldn't get element with ID '{elem_id}'"));
    }

    elem_maybe
}

/// Gets an [[`Element`]] by its `elem_id`, if unsuccessful, panics with an error message.
pub fn get_elem_or_panic(elem_id: ElementId) -> Element {
    get_elem(elem_id).unwrap_or_else(|| panic!("Expected element '{elem_id}' to exist"))
}

/// Gets the document object.
pub fn get_document() -> Document {
    window()
        .expect("No global window exists")
        .document()
        .expect("There's no document in the window object")
}

/// Sets the `onclick` field of `elem` to `callback`.
pub fn set_onclick<F>(elem: &HtmlElement, callback: F)
where
    F: IntoWasmClosure<dyn Fn(Event)> + 'static,
{
    // https://stackoverflow.com/questions/60054963/how-to-convert-closure-to-js-sysfunction
    let closure = Closure::new(callback);
    elem.set_onclick(Some(closure.as_ref().unchecked_ref()));
    closure.forget();
}

