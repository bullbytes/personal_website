mod bit;
mod nibble;
mod html_util;
mod page_access;

use nibble::Nibble;
use crate::nibble::SignStrategy;
use html_util::{
     log_to_browser, add_class, query_selector, new_simple_td, new_table_row_with_data,
};
use wasm_bindgen::JsCast;
use html_util::get_elem_or_panic as get;

use page_access::{ElementId, Css};
use wasm_bindgen::prelude::{wasm_bindgen, JsValue};
use web_sys::{js_sys::Array, Element, Event, HtmlElement};
use crate::html_util::{get_document, set_onclick};

// Called when the Wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    // Show Rust errors and stack traces in the browser console
    console_error_panic_hook::set_once();

    // Unsigned integers
    get_bit_elements_little_endian_of(ElementId::BitsOfUnsignedInteger)
        .iter().for_each(add_on_click_for_bits_of_unsigned_integer);

    // This table shows all possible values of a nibble if it represents an unsigned integer
    fill_unsigned_integers_table_with_all_values()?;

    // Signed integers with sign bit
    get_bit_elements_little_endian_of(ElementId::BitsOfIntegerWithSignBit)
        .iter().for_each(add_on_click_for_bits_of_signed_integer_with_sign_bit);

    fill_signed_integers_with_sign_bit_table_with_all_values()?;

    // Ones' complement
    get_bit_elements_little_endian_of(ElementId::BitsOfIntegerWithOnesComplement)
        .iter().for_each(add_on_click_for_bits_of_signed_integer_with_ones_complement);

    fill_signed_integers_with_ones_complement_table_with_all_values()?;

    // Shows the calculation of how to get the ones' complement of a number by subtracting it
    // from 15 (1111 in binary)
    get_bit_elements_little_endian_of(ElementId::BitsOfNumberToOnesComplement)
        .iter().for_each(add_on_click_for_bits_in_calculation_of_ones_complement);


    // Two's complement
    get_bit_elements_little_endian_of(ElementId::BitsOfIntegerWithTwosComplement)
        .iter().for_each(add_on_click_for_bits_of_signed_integer_with_twos_complement);

    fill_signed_integers_with_twos_complement_table_with_all_values()?;

    // Shows the calculation of how to get the two's complement of a number by subtracting it
    // from 16 (1 0000 in binary)
    get_bit_elements_little_endian_of(ElementId::BitsOfNumberToTwosComplement)
        .iter().for_each(add_on_click_for_bits_in_calculation_of_twos_complement);

    Ok(())
}

fn add_on_click_for_bits_of_signed_integer_with_twos_complement(bit_elem: &HtmlElement) {
    set_onclick(bit_elem, |event: Event| {
        toggle_bit(get_target(&event));

        let decimal = bit_elements_to_decimal(ElementId::BitsOfIntegerWithTwosComplement, SignStrategy::TwosComplement);
        get(ElementId::DecimalResultOfIntegerWithTwosComplement).set_text_content(Some(&decimal));
    });
}

fn fill_signed_integers_with_twos_complement_table_with_all_values() -> Result<(), JsValue> {
    let signed_integers_table_with_all_values = get(ElementId::SignedIntegersWithTwosComplementTableWithAllValues);

    // Each column has a header row with showing where the sign bit is and which decimal values the
    // bits have
    let header_data_for_one_column = vec!["-8", "4", "2", "1"];
    let header_data_for_two_columns = [header_data_for_one_column.clone(),
                                            // The filler data between columns is two cells: one
                                            // for the equal sign, one for the decimal result
                                            vec![" "," "],
                                            header_data_for_one_column].concat();

    let bit_values_in_decimal_row_top = &new_table_row_with_data(&header_data_for_two_columns)?;
    add_class(Css::BitValuesInDecimal, bit_values_in_decimal_row_top);
    signed_integers_table_with_all_values.append_child(bit_values_in_decimal_row_top)?;

    // Add a row of two nibbles with decimal result for each possible value of a nibble.
    // We only go up to 7 (0111 in binary, half the way) and then flip the most significant bit
    // of the nibble each time to get the nibble representing negative values. Then, we add both the positive and negative table to the row.
    for decimal in 0..=7 {
        let row_with_two_nibbles = get_document().create_element("tr")?;
        add_class(Css::BitsWithResult, &row_with_two_nibbles);

        let nibble = Nibble::from(decimal);
        row_with_two_nibbles.append_with_node(&nibble_to_bit_table_data_with_decimal_result(&nibble, SignStrategy::TwosComplement, None))?;

        let negative_nibble = nibble.flip_most_significant_bit();
        row_with_two_nibbles.append_with_node(&nibble_to_bit_table_data_with_decimal_result(&negative_nibble, SignStrategy::TwosComplement, None))?;

        signed_integers_table_with_all_values.append_child(&row_with_two_nibbles)?;
    }
    // Add the decimal values of each bit again for better readability
    let bit_values_in_decimal_row_bottom = deep_copy(bit_values_in_decimal_row_top, "bitValuesInDecimalBottom")?;
    signed_integers_table_with_all_values.append_child(&bit_values_in_decimal_row_bottom)?;

    Ok(())
}

fn add_on_click_for_bits_in_calculation_of_twos_complement(bit_elem: &HtmlElement) {
    set_onclick(bit_elem, |event: Event| {
        toggle_bit(get_target(&event));

        // Show the decimal value of the number to negate with two's complement
        get(ElementId::CommentDecimalToNegateWithTwosComplement).set_text_content(Some(
                &bit_elements_to_decimal(ElementId::BitsOfNumberToTwosComplement, SignStrategy::TwosComplement)));

        set_twos_complement_in_calculation();
    });
}

fn get_html_elem_by_id(id: &str) -> HtmlElement {
    let elem = get_document().get_element_by_id(id)
        .unwrap_or_else(|| panic!("Could not find element with ID '{id}'"));
    elem.dyn_into().unwrap_or_else(|_| panic!("Could not convert element with ID {id} to HtmlElement"))
}

fn set_twos_complement_in_calculation() {
    let bit_elements_as_u8s = get_bit_elements_little_endian_of(ElementId::BitsOfNumberToTwosComplement)
        .iter().map(elem_as_u8).collect::<Vec<u8>>();
    let nibble = Nibble::from_little_endian_u8s(&bit_elements_as_u8s);
    let twos_complement = nibble.flip().add_one();
    let result_bits_prefix = "twosComplementResultIndex";
    twos_complement.bits_little_endian().iter().enumerate().for_each(|(index, bit)|
        {
            let elem_id_of_result_bit = format!("{result_bits_prefix}{index}");
            let bit_elem = get_html_elem_by_id(&elem_id_of_result_bit);
            bit_elem.set_text_content(Some(&bit.to_string()));
        }
    );
    // Since we subtract the nibble from a five bit number ( 1 0000 in binary, 16 in decimal), the
    // most significant bit (index 4) is only 1 if the nibble was 0000
    let most_significant_result_bit_value = if nibble.is_zero() {
        "1"
    } else {
        "0"
    };

    get_html_elem_by_id(&format!("{result_bits_prefix}4")).set_text_content(Some(most_significant_result_bit_value));

    // Show the decimal value of the two's complement of the number as a comment next to the nibble
    get(ElementId::CommentTwosComplementAsDecimal).set_text_content(Some(&twos_complement.to_decimal(SignStrategy::TwosComplement)));
}

fn add_on_click_for_bits_in_calculation_of_ones_complement(bit_elem: &HtmlElement) {
    set_onclick(bit_elem, |event: Event| {
        toggle_bit(get_target(&event));

        // Show the decimal value of the number to negate with ones' complement
        get(ElementId::CommentDecimalToNegateWithOnesComplement).set_text_content(Some(
                &bit_elements_to_decimal(ElementId::BitsOfNumberToOnesComplement, SignStrategy::OnesComplement)));

        set_ones_complement_in_calculation();
    });
}

fn bit_elements_to_decimal(bit_elements_parent_id: ElementId, sign_strategy: SignStrategy) -> String {
    let bit_elements_as_u8s: Vec<u8> = get_bit_elements_little_endian_of(bit_elements_parent_id)
        .iter().map(elem_as_u8).collect();
    let nibble = Nibble::from_little_endian_u8s(&bit_elements_as_u8s);

    nibble.to_decimal(sign_strategy)
}

fn set_ones_complement_in_calculation() {
    let bit_elements_as_u8s = get_bit_elements_little_endian_of(ElementId::BitsOfNumberToOnesComplement)
        .iter().map(elem_as_u8).collect::<Vec<u8>>();
    let nibble = Nibble::from_little_endian_u8s(&bit_elements_as_u8s);
    let ones_complement = nibble.flip();
    ones_complement.bits_little_endian().iter().enumerate().for_each(|(index, bit)|
        {
            let elem_id_of_result_bit = format!("onesComplementResultIndex{index}");
            let bit_elem = get_html_elem_by_id(&elem_id_of_result_bit);
            bit_elem.set_text_content(Some(&bit.to_string()));
        }
    );

    // Show the decimal value of the ones' complement of the number as a comment next to the nibble
    get(ElementId::CommentOnesComplementAsDecimal).set_text_content(Some(&ones_complement.to_decimal(SignStrategy::OnesComplement)));
}

fn nibble_to_bit_table_data_with_decimal_result(nibble: &Nibble, sign_strategy: SignStrategy, msb_class_maybe: Option<Css>) -> Array {

        let array_of_table_data = Array::new();
        nibble.bits_big_endian().iter().enumerate().for_each(|(index, bit)|
            {
                let bit_as_td = new_simple_td(bit);

                // Since we're going through the bits in big endian order, the first bit is the sign bit
                if index == 0 {
                    if let Some(msb_class) = msb_class_maybe {
                        add_class(msb_class, &bit_as_td);
                    }
                }
                array_of_table_data.push(&bit_as_td);
            }
        );
        array_of_table_data.push(add_class(Css::EqualSign, &new_simple_td("=")));
        let decimal_result = nibble.to_decimal(sign_strategy);
        array_of_table_data.push(add_class(Css::DecimalResult, &new_simple_td(&decimal_result)));
        array_of_table_data
}

fn fill_signed_integers_with_ones_complement_table_with_all_values() -> Result<(), JsValue> {
    let signed_integers_table_with_all_values = get(ElementId::SignedIntegersWithOnesComplementTableWithAllValues);
    // Each column has a header row with showing where the sign bit is and which decimal values the
    // bits have
    let header_data_for_one_column = vec!["-7", "4", "2", "1"];
    let header_data_for_two_columns = [header_data_for_one_column.clone(),
                                            // The filler data between columns is two cells: one
                                            // for the equal sign, one for the decimal result
                                            vec![" "," "],
                                            header_data_for_one_column].concat();

    let bit_values_in_decimal_row_top = &new_table_row_with_data(&header_data_for_two_columns)?;
    add_class(Css::BitValuesInDecimal, bit_values_in_decimal_row_top);
    signed_integers_table_with_all_values.append_child(bit_values_in_decimal_row_top)?;

    // Add a row of bits for each possible value of a nibble
    // We only go up to 7 (0111 in binary, half the way) since we flip the nibble each time and put the unflipped and flipped bits
    // in the same table row.
    for decimal in 0..=7 {
        let row_with_two_nibbles_and_decimal_result = get_document().create_element("tr")?;
        add_class(Css::BitsWithResult, &row_with_two_nibbles_and_decimal_result);

        let nibble = Nibble::from(decimal);
        let array_of_table_data_from_nibble = nibble_to_bit_table_data_with_decimal_result(&nibble, SignStrategy::OnesComplement, None);

        let array_of_table_data_from_flipped_nibble = nibble_to_bit_table_data_with_decimal_result(&nibble.flip(), SignStrategy::OnesComplement, None);

        row_with_two_nibbles_and_decimal_result.append_with_node(&array_of_table_data_from_nibble)?;
        row_with_two_nibbles_and_decimal_result.append_with_node(&array_of_table_data_from_flipped_nibble)?;

        signed_integers_table_with_all_values.append_child(&row_with_two_nibbles_and_decimal_result)?;
    }
    // Add the decimal values of each bit again for better readability
    let bit_values_in_decimal_row_bottom = deep_copy(bit_values_in_decimal_row_top, "bitValuesInDecimalBottom")?;
    signed_integers_table_with_all_values.append_child(&bit_values_in_decimal_row_bottom)?;

    Ok(())
}

fn add_on_click_for_bits_of_signed_integer_with_ones_complement(bit_elem: &HtmlElement) {
    set_onclick(bit_elem, |event: Event| {
        toggle_bit(get_target(&event));
        let signed_decimal = bit_elements_to_decimal(ElementId::BitsOfIntegerWithOnesComplement, SignStrategy::OnesComplement);
        get(ElementId::DecimalResultOfIntegerWithOnesComplement).set_text_content(Some(&signed_decimal));
    });
}

fn fill_signed_integers_with_sign_bit_table_with_all_values() -> Result<(), JsValue> {
    let signed_integers_table_with_all_values = get(ElementId::SignedIntegersWithSignBitTableWithAllValues);

    // Each column has a header row with showing where the sign bit is and which decimal values the
    // bits have
    let header_data_for_one_column = vec!['-', '4', '2', '1'];
    let header_data_for_two_columns = [header_data_for_one_column.clone(),
                                            // The filler data between columns is two cells: one
                                            // for the equal sign, one for the decimal result
                                            vec![' ',' '],
                                            header_data_for_one_column].concat();

    let bit_values_in_decimal_row_top = &new_table_row_with_data(&header_data_for_two_columns)?;
    add_class(Css::BitValuesInDecimal, bit_values_in_decimal_row_top);
    signed_integers_table_with_all_values.append_child(bit_values_in_decimal_row_top)?;

    // Add a row of two nibbles with decimal result for each possible value of a nibble.
    // We only go up to 7 (0111 in binary, half the way) since we flip the most significant bit (the sign bit) of the nibble each time and put the bits of both nibbles in the same table row.
    for decimal in 0..=7 {
        let row_with_two_nibbles = get_document().create_element("tr")?;
        add_class(Css::BitsWithResult, &row_with_two_nibbles);

        let nibble = Nibble::from(decimal);
        row_with_two_nibbles.append_with_node(&nibble_to_bit_table_data_with_decimal_result(&nibble, SignStrategy::SignBit, Some(Css::SignBit)))?;

        let negative_nibble = nibble.flip_most_significant_bit();
        row_with_two_nibbles.append_with_node(&nibble_to_bit_table_data_with_decimal_result(&negative_nibble, SignStrategy::SignBit, Some(Css::SignBit)))?;

        signed_integers_table_with_all_values.append_child(&row_with_two_nibbles)?;
    }
    // Add the decimal values of each bit again for better readability
    let bit_values_in_decimal_row_bottom = deep_copy(bit_values_in_decimal_row_top, "bitValuesInDecimalBottom")?;
    signed_integers_table_with_all_values.append_child(&bit_values_in_decimal_row_bottom)?;
    Ok(())
}

fn add_on_click_for_bits_of_signed_integer_with_sign_bit(bit_elem: &HtmlElement) {
    set_onclick(bit_elem, |event: Event| {
        toggle_bit(get_target(&event));

        let decimal = bit_elements_to_decimal(ElementId::BitsOfIntegerWithSignBit, SignStrategy::SignBit);
        get(ElementId::DecimalResultOfIntegerWithSignBit).set_text_content(Some(&decimal));
    });
}

fn add_on_click_for_bits_of_unsigned_integer(bit_elem: &HtmlElement) {
    set_onclick(bit_elem, |event: Event| {
        toggle_bit(get_target(&event));

        let decimal =bit_elements_to_decimal(ElementId::BitsOfUnsignedInteger, SignStrategy::Unsigned);
        get(ElementId::DecimalResultOfUnsignedInteger).set_text_content(Some(&decimal));
    });
}

fn toggle_bit(bit_elem: HtmlElement) {
    let new_bit =  if bit_elem.text_content() == Some("0".to_string()) {
        Some("1")
    } else {
        Some("0")
    };
    bit_elem.set_text_content(new_bit);
}

fn elem_as_u8(elem: &HtmlElement) -> u8 {
    elem.text_content().unwrap().parse::<u8>().unwrap()
}

fn fill_unsigned_integers_table_with_all_values() -> Result<(), JsValue> {
    let unsigned_integers_table_with_all_values = get(ElementId::UnsignedIntegersTableWithAllValues);

    // Each column has a header row with showing which decimal values the bits have
    let header_data_for_one_column = vec!['8', '4', '2', '1'];
    let header_data_for_two_columns = [header_data_for_one_column.clone(),
                                            // The filler data between columns is two cells: one
                                            // for the equal sign, one for the decimal result
                                            vec![' ',' '],
                                            header_data_for_one_column].concat();

    let bit_values_in_decimal_row_top = &new_table_row_with_data(&header_data_for_two_columns)?;
    add_class(Css::BitValuesInDecimal, bit_values_in_decimal_row_top);
    unsigned_integers_table_with_all_values.append_child(bit_values_in_decimal_row_top)?;

    // Add a row of two nibbles with decimal result for each possible value of a nibble.
    // We only go up to 7 (0111 in binary, half the way) since we flip the most significant bit of the nibble each time and put the bits of both nibbles in the same table row.
    for decimal in 0..=7 {
        let row_with_two_nibbles = get_document().create_element("tr")?;
        add_class(Css::BitsWithResult, &row_with_two_nibbles);

        let nibble = Nibble::from(decimal);
        row_with_two_nibbles.append_with_node(&nibble_to_bit_table_data_with_decimal_result(&nibble, SignStrategy::Unsigned, None))?;

        let negative_nibble = nibble.flip_most_significant_bit();
        row_with_two_nibbles.append_with_node(&nibble_to_bit_table_data_with_decimal_result(&negative_nibble, SignStrategy::Unsigned, None))?;

        unsigned_integers_table_with_all_values.append_child(&row_with_two_nibbles)?;
    }

    // Add the decimal values of each bit again for better readability
    let bit_values_in_decimal_row_bottom = deep_copy(bit_values_in_decimal_row_top, "bitValuesInDecimalBottom")?;
    unsigned_integers_table_with_all_values.append_child(&bit_values_in_decimal_row_bottom)?;
    Ok(())
}

fn deep_copy(original: &Element, new_id: &str) -> Result<Element, JsValue> {
    let copy = original.clone_node_with_deep(true)?.unchecked_into::<Element>();
    copy.set_attribute("id", new_id)?;
    Ok(copy)
}

/// Gets the four elements representing bits from inside `parent_id`.
/// The first element in the returned vector is the one with the lowest index.
fn get_bit_elements_little_endian_of(parent_id: ElementId) -> Vec<HtmlElement> {
    let mut bit_elems: Vec<HtmlElement> = vec![];
    let parent = get(parent_id);
    for idx in 0..4 {
        let bit_elem_selector = format!("#index{idx}");
        let bit_elem_maybe = query_selector(&parent, &bit_elem_selector);
        match bit_elem_maybe {
            None => {
                log_to_browser(format!("Could not find bit element inside {parent_id} with selector {bit_elem_selector}"));
            }
            Some(bit_elem) => {
                let bit_elem_id = bit_elem.id();
                bit_elems.push(bit_elem.dyn_into().unwrap_or_else(|_| panic!("Could not convert bit element with ID {bit_elem_id} inside {parent_id} with selector {bit_elem_selector} to HtmlElement")));
            }
        }
    }
    bit_elems
}

/// Gets the target of the `event` as a `T`.
/// Useful for event listeners where the target is known to be of type `T`.
fn get_target<T: JsCast>(event: &Event) -> T {
    let target = event.target().unwrap();
    target.dyn_into().unwrap()
}
