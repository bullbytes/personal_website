# Structure and progression of topics
1. Start with the big picture with little depth of what the Internet consists of: mb.sb/net
2. Go into details of LAN: Switches, MAC addresses, the switch's MAC address table, DHCP (which ends in the default gateway, the segue into routing)
3. Connect LANs via routers. Cover routing

# Design
For DHCP use a font or adorned text as SVG in the style of the ACDC logo:
  * https://www.fontbolt.com/font/ac-dc-font/
