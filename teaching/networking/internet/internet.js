const simpleFadeIn = [
      { opacity: 0 },
      { opacity: 1}
    ];

const routerColor = '#258579';
const switchColor = '#0088b8'

const animationTiming = {
    duration: 3000,
    // Keep the state of the SVG after the animation has finished,
    // both for playing it forward and in reverse
    fill: "both"
}
function applyStyleOfSingleState(state) {

  applyStyleOfStates([state]);

}

// Accumulates the styles of multiple states and applies them to their elements
// The style of later states takes precedence of states in earlier styles
function applyStyleOfStates(states) {

  // The styles (the map's value) is another map from style name to style value:
  // {
  //   element1 → { opacity → 1,
  //                fill → blue,
  //             },
  //   element2 → { opacity → 0,
  //                fill → red,
  //             }
  // }
  const elementsAndFinalStyles = new Map();

  states.forEach((elementsAndStylesArray, i) => {
    // An elementsAndStylesArray can look like this:
    // [{styles: { opacity: 0 }, elements: [el1, el2, el3]}, {styles: { fill: "red"}, elements: el1}];
    elementsAndStylesArray.forEach(elementsAndStyles => {
      const elements = elementsAndStyles.elements;
      const styles = elementsAndStyles.styles
      elements.forEach(elem => {
        // Get the style map of the element, initialize the map if the element hasn't one
        let styleMap = elementsAndFinalStyles.get(elem);
        if (styleMap === undefined) {
          styleMap = new Map();
        }

        for (var styleName in styles) {
          const styleValue = styles[styleName];
          styleMap.set(styleName, styleValue);
        }

        elementsAndFinalStyles.set(elem, styleMap)});
    });
  });

  elementsAndFinalStyles.forEach(applyStylesFromMap)
}

function applyStylesFromMap(styleMap, element, mapFromElementsToStylesMap) {

  for (var [styleName, styleValue] of styleMap) {
      // console.log(`${element.id}: setting ${styleName} to ${styleValue}`)
      element.style[styleName] = styleValue;
  }
}

function logMapElements(value, key, map) {
  console.log(`${key} = ${JSON.stringify(value)}`);
}


function playOrRevertSteps(steps, playStop) {

    for (let i = 0; i < steps.length; i++) {

      const stepAnimations = steps[i];
      // playOrRevertAnimation(stepAnimations, 0, playStop);

      // Play animations forward
      if (i < playStop) {
        stepAnimations.forEach(anim => {
          const target = anim.effect.target;
          const curPlayrate = anim.playbackRate;
          // console.log(`Forward play ${target.id}`);
          if (anim.playState === "running") {
            // Make sure the animation runs forwards which means showing the element
            anim.playbackRate = 1;
            // console.log("  set playback rate to 1");
          }
          else {
            if (anim.playState === "paused" && curPlayrate === 1) {
                anim.play();
                // console.log("  play it");
              }
            else if (anim.playState === "finished" && curPlayrate === -1) {
                // Make sure we play the animation forwards. If it has been played in reverse before, the playbackRate is -1
                anim.playbackRate = 1;
                anim.play();
                // console.log("  was finished, set rate to 1 and play");
            }
          }
        });
      }

      // Play animations in reverse to return them to the original state
      else {
        stepAnimations.forEach(anim => {
          const target = anim.effect.target;
          const curPlayrate = anim.playbackRate;
          // console.log(`Backward play ${target.id}. Play state is ${anim.playState}. Playback rate: ${curPlayrate}`);

          if (anim.playState === "running"){
            // Make sure the animation runs backwards which means hiding the element
            anim.playbackRate = -1;
          }
          else {
            if (anim.playState === "finished" && curPlayrate === 1) {
              // This sets the animation's playbackRate to -1 and plays it. Further calls to .play() will play it in reverse
              anim.reverse();
            }
          }
        });
      }
  }
}

// Add an animation to the element, pause it after adding.
function addAnimation (element, keyframes, animationDuration) {

  const animationTiming = {
    duration: animationDuration,
    // Keep the state of the SVG after the animation has finished,
    // both for playing it forward and in reverse
    fill: "both"
  }

  // Create the animation object but pause it immediately to prevent it from running on page load
  const animation = element.animate(
    keyframes,
    animationTiming
  );
  animation.pause();

  return animation;
}

// Gets the first "tspan" element of another element inside an SVG document.
function getTextSpanOf(elemName, svgDoc) {
  const elem = svgDoc.getElementById(elemName);
  if (elem) {
    return elem.getElementsByTagName("tspan")[0];
  }
  else {console.log(`Could not find element ${elemName}`);}
}
