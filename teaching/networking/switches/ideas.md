* Show what's in an [Ethernet frame](https://en.wikipedia.org/wiki/Ethernet_frame#Ethernet_II) after or before the section "MAC addresses":
  * MAC addresses
  * Next package (ARP, IPv4, IPv6)
  * Maybe checksum
* Show how ARP works, which would illustrate that broadcasts and unicast flooding look similar but the reason is different:
  * Flooding because destination MAC address is not in switch's MAC address table
  * Flooding because destination MAC address is the broadcast address
