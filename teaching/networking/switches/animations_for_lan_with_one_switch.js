function addAnimationsForLanWithOneSwitch () {

  lanWithSwitch.addEventListener('load', () => {
    // Get the SVGSVGElement: https://developer.mozilla.org/en-US/docs/Web/API/SVGSVGElement
    const svgElem = lanWithSwitch.contentDocument.rootElement;

    // Hide the paths along which the frames move
    svgElem.getElementById('package_animation_paths').style.visibility = 'hidden';

    // MAC addresses move along these paths
    svgElem.getElementById('frame_group_animation_paths').style.visibility = 'hidden';

    // Only show the flooding banner once the switch floods frames
    const floodingGroup = svgElem.getElementById('flooding_group');
    floodingGroup.style.visibility = 'hidden';

    // The text saying "read frame" at PC1. Only show it after the frame has arrived
    const readFrameText = svgElem.getElementById('reading_frame_text');
    const createNewFrameText = svgElem.getElementById('create_new_frame_text');
    readFrameText.style.visibility = 'hidden';
    createNewFrameText.style.visibility = 'hidden';

    // Show these MAC addresses once a frame with the MAC address as source enters a port
    const pc1MacAddress = svgElem.getElementById('pc_1_mac_address');
    pc1MacAddress.style.opacity = '0';
    const laptop1MacAddress = svgElem.getElementById('laptop_1_mac_address');
    laptop1MacAddress.style.opacity = '0';

    // Once laptop2 and PC2 receive a frame not destined for them, show a shrug emoji
    const shrugLaptop2 = svgElem.getElementById('shrug_laptop2');
    shrugLaptop2.style.opacity = '0';
    const shrugPc2 = svgElem.getElementById('shrug_pc2');
    shrugPc2.style.opacity = '0';

    const cableLaptop1toSwitchId = 'cable_laptop1_switch';

    const frameBetweenLaptop1AndPc1 = clone(svgElem.getElementById('package_1'), 'frame_between_laptop_1_and_pc_1');
    frameBetweenLaptop1AndPc1.setAttribute('transform', 'scale(0.5) translate(-12,-18)');

    // Duplicate the original frame since the switch floods the frame
    const frameFromSwitchToLaptop2 = clone(frameBetweenLaptop1AndPc1, 'frame_from_switch_to_laptop_2');
    frameFromSwitchToLaptop2.style.opacity = '0';
    const frameFromSwitchToPc2 = clone(frameBetweenLaptop1AndPc1, 'frame_from_switch_to_pc_2');
    frameFromSwitchToPc2.style.opacity = '0';

    // Add the frames as siblings to the path representing the cable
    [frameBetweenLaptop1AndPc1, frameFromSwitchToLaptop2, frameFromSwitchToPc2].forEach(
      frame => svgElem.getElementById(cableLaptop1toSwitchId).parentNode.appendChild(frame)
    );

    const animationIdPrefixForLaptop1ToPc1 = 'laptop_1_to_pc_1_';
    // We'll flood the frames at this point
    const goToSwitchCenterAnimEnd = animationIdPrefixForLaptop1ToPc1 + 'anim_path_port_1_to_switch_center_motion.end';

    // The function returns its last animation
    const lastAnimation = frameGoesFromLaptop1ToPc1(animationIdPrefixForLaptop1ToPc1);

    // Animations for frame going from switch to laptop 2 (via port 3)
    // The animations for making the frame visible and moving it start at the same time so it appears at the right location
    const showFrameForLaptop2 = createFadeInAnimation('showFrameForLaptop2', goToSwitchCenterAnimEnd);
    const animationIdPrefixForSwitchToLaptop2 = 'switch_to_laptop_2';
    const movementsForLaptop2 = moveOnPaths(animationIdPrefixForSwitchToLaptop2, ['anim_path_for_package_to_laptop2', 'anim_path_go_to_port_3', 'cable_laptop2_switch'], goToSwitchCenterAnimEnd);
    const disappearAtLaptop2 = createFadeOutAnimation('makeFrameAtLaptop2Disappear', animationIdPrefixForSwitchToLaptop2 + 'cable_laptop2_switch_motion.end');
    [showFrameForLaptop2, movementsForLaptop2, disappearAtLaptop2].flat().forEach(movement => frameFromSwitchToLaptop2.appendChild(movement));

    const fadeInShrugAtLaptop2 = createFadeInAnimation('fadeInShrugAtLaptop2', animationIdPrefixForSwitchToLaptop2 + 'cable_laptop2_switch_motion.end');
    const fadeOutShrugAtLaptop2 = createFadeOutAnimation('fadeOutShrugAtLaptop2', 'fadeInShrugAtLaptop2.end + 1s');
    shrugLaptop2.appendChild(fadeInShrugAtLaptop2);
    shrugLaptop2.appendChild(fadeOutShrugAtLaptop2);

    // Animations for frame going from switch to PC 2 (via port 4)
    const showFrameForPc2 = createFadeInAnimation('showFrameForPc2', goToSwitchCenterAnimEnd);
    const animationIdPrefixForSwitchToPc2 = 'switch_to_pc_2';
    const movementsForPc2 = moveOnPaths(animationIdPrefixForSwitchToPc2, ['anim_path_for_package_to_pc2', 'anim_path_go_to_port_4', 'cable_pc2_switch'], goToSwitchCenterAnimEnd);
    const disappearAtPc2 = createFadeOutAnimation('makeFrameAtPc2Disappear', animationIdPrefixForSwitchToPc2 + 'cable_pc2_switch_motion.end');
    [showFrameForPc2, movementsForPc2, disappearAtPc2].flat().forEach(movement => frameFromSwitchToPc2.appendChild(movement));

    const fadeInShrugAtPc2 = createFadeInAnimation('fadeInShrugAtPc2', animationIdPrefixForSwitchToPc2 + 'cable_pc2_switch_motion.end');
    const fadeOutShrugAtPc2 = createFadeOutAnimation('fadeOutShrugAtPc2', 'fadeInShrugAtPc2.end + 1s');
    shrugPc2.appendChild(fadeInShrugAtPc2);
    shrugPc2.appendChild(fadeOutShrugAtPc2);

    svgElem.pauseAnimations();

    const slider = document.getElementById('oneSwitchSlider');
    slider.min = 0;
    slider.value = 0;
    // Calculate total animation duration in seconds
    const totalDurationInSeconds = lastAnimation.getStartTime() + lastAnimation.getSimpleDuration();
    slider.max = totalDurationInSeconds;

    slider.addEventListener('input', () => svgElem.setCurrentTime(slider.value));

    function frameGoesFromLaptop1ToPc1(animationIdPrefix) {
      // Animations for frame from laptop 1 to PC 1
      const goFromLaptopToSwitch = newAnimMotionWithMpath(cableLaptop1toSwitchId, objToMap({'dur': '2s', 'id': 'goFromLaptopToSwitch'}));

      // Once the frame has moved from laptop 1 to the switch, we show the laptop's MAC address in the switch's MAC address table
      laptop1MacAddress.appendChild(createFadeInAnimation('showLaptop1MacAddressInTable', 'goFromLaptopToSwitch.end'));

      const goFromSwitchToPc1 = moveOnPaths(animationIdPrefix, ['anim_path_port_1_to_switch_center', 'anim_path_original_package_moves_aside_for_cloning', 'anim_path_switch_to_port_2', 'cable_pc1_switch', 'anim_path_from_cable_to_center_of_pc1'], 'goFromLaptopToSwitch.end')

      // Once the frame has moved to the center of the switch, we show the flooding banner
      floodingGroup.appendChild(createSetVisible(goToSwitchCenterAnimEnd));
      floodingGroup.appendChild(createSetHidden(animationIdPrefix + 'cable_pc1_switch_motion.end'));

      // "read frame" appears at PC1 once the frame has arrived. The text disappears once the frame has disappeared
      readFrameText.appendChild(createSetVisible(animationIdPrefix + 'cable_pc1_switch_motion.end'));
      readFrameText.appendChild(createSetHidden('disappearAtPc1.end'));

      const disappearAtPc1 = createFadeOutAnimation('disappearAtPc1', animationIdPrefix + 'anim_path_from_cable_to_center_of_pc1_motion.end');
      const reappearAtPc1 = createFadeInAnimation('reappearAtPc1', 'disappearAtPc1.end');

      const lastAnimationOfSwappingMacAddresses = swapSourceAndDestinationAddressInEthernetFrame(svgElem.getElementById('source_mac_address_group'), svgElem.getElementById('destination_mac_address_group'), animationIdPrefix, 'disappearAtPc1.end');

      createNewFrameText.appendChild(createSetVisible('reappearAtPc1.begin'));
      createNewFrameText.appendChild(createSetHidden(`${lastAnimationOfSwappingMacAddresses.id}.end`));

      // Once the frame has moved from PC 1 to the switch, we show the PC's MAC address in the switch's MAC address table
      const showPc1MacAddressInTable = createFadeInAnimation('showPc1MacAddressInTable', animationIdPrefix + 'cable_pc1_switch_reversed.end');
      pc1MacAddress.appendChild(showPc1MacAddressInTable);

      const moveBackToLaptop1 = moveOnPaths(animationIdPrefix, ['anim_path_from_cable_to_center_of_pc1', 'cable_pc1_switch', 'anim_path_switch_to_port_2', 'anim_path_original_package_moves_aside_for_cloning', 'anim_path_port_1_to_switch_center', cableLaptop1toSwitchId], `${lastAnimationOfSwappingMacAddresses.id}.end`, true);

      const allAnimationsBetweenLaptop1AndPc1 = [goFromLaptopToSwitch, goFromSwitchToPc1, disappearAtPc1, reappearAtPc1, moveBackToLaptop1].flat();

      allAnimationsBetweenLaptop1AndPc1.forEach(movement => frameBetweenLaptop1AndPc1.appendChild(movement));

      // Get the last animation which will be used to calculate the total duration of the animations
      return allAnimationsBetweenLaptop1AndPc1.at(-1);
    }
  });
}
