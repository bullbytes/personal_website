
function clone(oldElem, newId){
  const deepCopy = true;
  const newElem = oldElem.cloneNode(deepCopy);
  newElem.id = newId;
  return newElem;
}

// Converts an object like "{'key1': 'value1', 'key2': 'value2'}" into a Map object
function objToMap(obj){
  return new Map(Object.entries(obj));
}

// Creates a new SVG element with the given elemName
// attributeNamesAndValues is a Map object
function newSvgElem(elemName, attributeNamesAndValues) {

  const svgNs = "http://www.w3.org/2000/svg";
  const elem = document.createElementNS(svgNs, elemName);

  for (const [name, value] of attributeNamesAndValues) {
    elem.setAttribute(name, value);
  }
  return elem;
}

// Creates an animateMotion SVG element with an mpath for moving along a path.
// attributeNamesAndValues is a Map object
function newAnimMotionWithMpath(pathId, attributeNamesAndValues) {
  const animateMotion = newSvgElem('animateMotion', attributeNamesAndValues);
  const mpath = newSvgElem ('mpath', objToMap({href: `#${pathId}`}));
  animateMotion.appendChild(mpath);
  return animateMotion;
}
function createFadeInAnimation(elemId, begin){
  return newSvgElem('animate', objToMap({'id': elemId, 'dur': '1s', 'attributeName': 'opacity', 'from': '0', 'to': '1', 'begin': begin, 'fill': 'freeze'}));
}
function createFadeOutAnimation(elemId, begin){
  return newSvgElem('animate', objToMap({'id': elemId, 'dur': '1s', 'attributeName': 'opacity', 'from': '1', 'to': '0', 'begin': begin, 'fill': 'freeze'}));
}

function createSetVisible(begin){
  return newSvgElem('set', objToMap({'attributeName': 'visibility', 'to': 'visible', 'begin': begin}));
}

function createSetHidden(begin){
  return newSvgElem('set', objToMap({'attributeName': 'visibility', 'to': 'hidden', 'begin': begin}));
}

// Makes the sourceMac and destinationMac SVG elements swap places using the hardcoded IDs of paths in the SVG
// Returns the animation that runs last
function swapSourceAndDestinationAddressInEthernetFrame(sourceMac, destinationMac, animationIdPrefix, begin) {

  function removeTranslate(){
    return newSvgElem('animateTransform', objToMap({'attributeName': 'transform', 'type': 'translate', 'values': '0 0', 'begin': begin}));
  }

  const animateSourceMac = newAnimMotionWithMpath('anim_path_source_mac_address', objToMap({'dur': '2s', 'id': animationIdPrefix + 'animateSourceMac', 'begin': begin, 'fill': 'freeze'}));
  // We set the "translate" value to zero to make the movement along the animation path correct, otherwise there'd be an offset
  const removeTranslateForSrcMac = removeTranslate(begin);

  sourceMac.appendChild(removeTranslateForSrcMac);
  sourceMac.appendChild(animateSourceMac);

  const animateDestinationMac = newAnimMotionWithMpath('anim_path_destination_mac_address', objToMap({'dur': '2s', 'id': animationIdPrefix + 'animateDestinationMac', 'begin': begin, 'fill': 'freeze'}));
  const removeTranslateForDestMac = removeTranslate();

  destinationMac.appendChild(removeTranslateForDestMac);
  destinationMac.appendChild(animateDestinationMac);
  return animateDestinationMac;
}

// Returns an array of animateMotion objects that animate along the given paths, one after the other. "begin" is the begin of the first animation.
// If "reversed" is true, the animated SVG element will move backwards along the paths.
// The IDs of the forwards animations are the animationIdPrefix + their path ID + '_motion'. For reversed animations it's path ID + '_reversed'.
function moveOnPaths(animationIdPrefix, pathIds, begin, reversed = false) {

  var currentBegin = begin;
  var animParams = objToMap({'dur': '2s', 'fill': 'freeze'});
  var animationIdSuffix = '_motion';
  if (reversed) {
    animationIdSuffix = '_reversed';
    animParams.set('keyPoints', '1;0');
    animParams.set('keyTimes', '0;1');
    animParams.set('calcMode', 'linear');
  }

  return pathIds.map(pathId => {
    // It's important that the animation ID and the path ID are different since the animation won't run if its ID is duplicated in the SVG
    var animationId = animationIdPrefix + pathId + animationIdSuffix;
    animParams.set('id', animationId);
    animParams.set('begin', currentBegin);
    var currentAnim = newAnimMotionWithMpath(pathId, animParams);
    currentBegin = animationId + '.end';
    return currentAnim;
  });
}

