function addAnimationsForLanWithTwoSwitches(){

  lanWithTwoSwitches.addEventListener('load', () => {
    // Get the SVGSVGElement: https://developer.mozilla.org/en-US/docs/Web/API/SVGSVGElement
    const svgElem = lanWithTwoSwitches.contentDocument.rootElement;
    hideElements()

    function cloneLargeEthernetFrameAndMakeItSmall(idOfClonedFrame) {

      const clonedFrame = clone(get('frame_package'), idOfClonedFrame);
      clonedFrame.setAttribute('transform', 'scale(0.5) translate(-12,-18)');
      return clonedFrame;
    }

    // const bigFrameForRoundTripBetweenPcs = clone(get('package_1'), 'big_frame_for_round_trip_between_pcs);

    const lastAnimationOfRoundTripBetweenLaptops = sendFrameFromLaptop1ToLaptop2AndBack();
    const lastAnimationOfChangingMacAddresses = changeMacAddressesForNewFrame(`${lastAnimationOfRoundTripBetweenLaptops.id}.begin`);
    const lastAnimationOfRoundTripBetweenPcs = sendFrameFromPc1ToPc2AndBack(`${lastAnimationOfChangingMacAddresses.id}.end +1s`);

    const lastAnimation = lastAnimationOfRoundTripBetweenPcs;

    svgElem.pauseAnimations();

    const slider = document.getElementById('twoSwitchesSlider');
    slider.min = 0;
    slider.value = 0;
    const totalDurationInSeconds = lastAnimation.getStartTime() + lastAnimation.getSimpleDuration();

    slider.max = totalDurationInSeconds;

    slider.addEventListener('input', () =>  svgElem.setCurrentTime(slider.value));

    // Some elements should be hidden at the start of the animation
    function hideElements() {

      // Hide the paths along which the frames move
      setHidden(get('frame_animation_paths'));

      get('switch_1_table_mac_addresses').childNodes.forEach(setHidden);
      get('switch_2_table_mac_addresses').childNodes.forEach(setHidden);

      // MAC addresses move along these paths, don't show them
      setHidden(get('laptop_frame_group_animation_paths'));
      setHidden(get('pcs_frame_group_animation_paths'));

      get('pcs_frame_group').style.opacity = 0;

      // Only show the flooding banners once the switch floods frames
      setHidden(get('flooding_group_switch_1'));
      setHidden(get('flooding_group_switch_2'));

      // We'll highlight the MAC addresses of the PCs once they send frames to each other
      get('mac_address_pc_1_background').style.opacity = 0;
      get('mac_address_pc_2_background').style.opacity = 0;

    }
    function setHidden(elem){
      elem.style.visibility = 'hidden';
    }

    function get(elemId) {
      return svgElem.getElementById(elemId);
    }

    function sendFrameFromLaptop1ToLaptop2AndBack() {

      const frameFromLaptop1ToLaptop2 = cloneLargeEthernetFrameAndMakeItSmall('frame_from_laptop_1_to_laptop_2');

      // Duplicate the original frame since the switch floods the frame
      const frameForPc1 = clone(frameFromLaptop1ToLaptop2, 'frame_from_switch_to_pc_1');
      frameForPc1.style.opacity = '0';

      const frameForPc2 = clone(frameFromLaptop1ToLaptop2, 'frame_from_switch_to_pc_2');
      frameForPc2.style.opacity = '0';

      // Add the frames as siblings to the path representing the cable
      [frameFromLaptop1ToLaptop2, frameForPc1, frameForPc2].forEach(
        frame => get('cable_laptop1_switch').parentNode.appendChild(frame)
      );

      const animationIdPrefix = 'laptop_1_to_laptop_2_';
      const goFromLaptop1ToSwitch = newAnimMotionWithMpath('cable_laptop1_switch', objToMap({'dur': '2s', 'id': animationIdPrefix + 'goFromLaptop1ToSwitch'}));
      const goFromSwitchToLaptop2 = moveOnPaths(animationIdPrefix, ['anim_path_port_1_to_switch_center', 'anim_path_from_center_to_above_port_4', 'anim_path_go_to_port_4', 'cable_switch_1_to_switch_2', 'port_1_to_switch_2_center', 'switch_2_anim_path_go_above_port_2', 'switch_2_anim_path_go_to_port_2', 'cable_laptop2_switch', 'go_inside_laptop_2'], `${goFromLaptop1ToSwitch.id}.end`);

      // Once the frame has moved from laptop 1 to switch 1, we show the laptop's MAC address in the switch's MAC address table
      get('switch_1_laptop_1_mac_address').appendChild(createSetVisible(`${goFromLaptop1ToSwitch.id}.end`));

      // Each time the frame has moved to the center of the switches, we show the flooding banner
      get('flooding_group_switch_1').appendChild(createSetVisible(animationIdPrefix + 'anim_path_port_1_to_switch_center_motion.end'));
      get('flooding_group_switch_2').appendChild(createSetVisible(animationIdPrefix + 'port_1_to_switch_2_center_motion.end'));
      // Once it arrives at a host that discards the frame, we hide the banner
      get('flooding_group_switch_1').appendChild(createSetHidden(animationIdPrefix + 'cable_pc1_switch_motion.end'));
      get('flooding_group_switch_2').appendChild(createSetHidden(animationIdPrefix + 'cable_pc2_switch_motion.end'));

      // Show the MAC address of laptop 1 once it arrives at switch 2
      get('switch_2_laptop_1_mac_address').appendChild(createSetVisible(animationIdPrefix + 'cable_switch_1_to_switch_2_motion.end'));

      const disappearAtLaptop2 = createFadeOutAnimation(animationIdPrefix + 'disappearAtLaptop2', animationIdPrefix + 'go_inside_laptop_2_motion.end');
      const reappearAtLaptop2 = createFadeInAnimation(animationIdPrefix + 'reappearAtLaptop2', `${disappearAtLaptop2.id}.end`);

      // We swap source and destination when the frame reaches its first destination
      const lastAnimationOfSwappingMacAddresses = swapSourceAndDestinationAddressInEthernetFrame(get('laptops_source_mac_address_group'), get('laptops_destination_mac_address_group'), animationIdPrefix, `${disappearAtLaptop2.id}.end`);

      const goBackToLaptop1 = moveOnPaths(animationIdPrefix, ['go_inside_laptop_2', 'cable_laptop2_switch', 'switch_2_anim_path_go_to_port_2', 'switch_2_anim_path_go_above_port_2', 'port_1_to_switch_2_center', 'cable_switch_1_to_switch_2', 'anim_path_go_to_port_4', 'anim_path_from_center_to_above_port_4', 'anim_path_port_1_to_switch_center', 'cable_laptop1_switch'], `${lastAnimationOfSwappingMacAddresses.id}.end`, true);

      // Once the frame has moved from laptop 2 back to the switch 2, we show the laptop's MAC address in the switch's MAC address table
      get('switch_2_laptop_2_mac_address').appendChild(createSetVisible(animationIdPrefix + 'cable_laptop2_switch_reversed.end'));
      // Once the frame from laptop 2 has moved from switch 2 back to switch 1, we show laptop 2's MAC address in the MAC address table of switch 1
      get('switch_1_laptop_2_mac_address').appendChild(createSetVisible(animationIdPrefix + 'cable_switch_1_to_switch_2_reversed.end'));

      // The return frame from laptop 2 has arrived at laptop 1
      const disappearAtLaptop1 = createFadeOutAnimation(animationIdPrefix + 'disappearAtLaptop1', animationIdPrefix + 'cable_laptop1_switch_reversed.end');

      const allAnimationsOfFrameBetweenLaptops = [goFromLaptop1ToSwitch, goFromSwitchToLaptop2, disappearAtLaptop2, reappearAtLaptop2, goBackToLaptop1, disappearAtLaptop1].flat();

      allAnimationsOfFrameBetweenLaptops.forEach(movement => frameFromLaptop1ToLaptop2.appendChild(movement));

      // Animations for frame destined for PC 1 at switch 1 (via port 2)
      // The animations for making the frame visible and moving it start at the same time so it appears at the right location
      const showFrameForPc1Begin = animationIdPrefix + 'anim_path_port_1_to_switch_center_motion.end';
      const showFrameForPc1 = createFadeInAnimation('showFrameForPc1', showFrameForPc1Begin);
      const movementsOfFrameForPc1 = moveOnPaths(animationIdPrefix, ['anim_path_go_above_port_2', 'anim_path_switch_to_port_2', 'cable_pc1_switch'], showFrameForPc1Begin)
      const disappearAtPc1 = createFadeOutAnimation('disappearAtPc1', animationIdPrefix + 'cable_pc1_switch_motion.end');

      [showFrameForPc1, movementsOfFrameForPc1, disappearAtPc1].flat().forEach(movement => frameForPc1.appendChild(movement));

      // Animations for frame destined for PC 2 at switch 2 (via port 3)
      const showFrameForPc2Begin = animationIdPrefix + 'port_1_to_switch_2_center_motion.end';
      const showFrameForPc2 = createFadeInAnimation('showFrameForPc1', showFrameForPc2Begin);
      const movementsOfFrameForPc2 = moveOnPaths(animationIdPrefix, ['switch_2_anim_path_go_above_port_3', 'switch_2_anim_path_go_to_port_3', 'cable_pc2_switch'], showFrameForPc2Begin)
      const disappearAtPc2 = createFadeOutAnimation(animationIdPrefix + 'disappearAtPc2', animationIdPrefix + 'cable_pc2_switch_motion.end');
      [showFrameForPc2, movementsOfFrameForPc2, disappearAtPc2].flat().forEach(movement => frameForPc2.appendChild(movement));

      // Return the last animation so we can start other animations after it
      return disappearAtLaptop1;
    }

    function changeMacAddressesForNewFrame(beginOfAnimation){

      const animationIdPrefix = 'change_mac_addresses_for_new_frame_with_laptops';

      // Fade out the frame that shows MAC addresses
      const laptopsBigFrameFadeOut = createFadeOutAnimation(animationIdPrefix + 'fadeOutBigEthernetFrame', beginOfAnimation);
      get('laptops_frame_group').appendChild(laptopsBigFrameFadeOut);

      const fadeInFrame = createFadeInAnimation('fadeInFrame', `${laptopsBigFrameFadeOut.id}.end +1s`);
      get('pcs_frame_group').appendChild(fadeInFrame);
      // Also set the MAC addresses opacity to zero so we can fade them in after the whole Ethernet frame was faded in again
      // Mind that after the frame roundtrip between laptops, the source and destination MAC address group in the SVG are switched: `destination_mac_address_group` represents the source and `source_mac_address_group` is the destination
      get('pcs_destination_mac_address_group').appendChild(newSvgElem('set', objToMap({'attributeName': 'opacity', 'to': '0', 'begin': `${fadeInFrame.id}.begin`})));
      get('pcs_source_mac_address_group').appendChild(newSvgElem('set', objToMap({'attributeName': 'opacity', 'to': '0', 'begin': `${fadeInFrame.id}.begin`})));

      // When we fade in the MAC addresses beneath the PCs and in the big Ethernet frame
      const newMacAddressesFadeInStart = `${fadeInFrame.id}.end +1s`;

      // Fade out the highlighting background of the previous sender and recipient: the laptops
      get('mac_address_laptop_1_background').appendChild(createFadeOutAnimation(animationIdPrefix + 'fadeOutLaptop1MacAddressBackground', beginOfAnimation));
      get('mac_address_laptop_2_background').appendChild(createFadeOutAnimation(animationIdPrefix + 'fadeOutLaptop2MacAddressBackground', beginOfAnimation));

      // Fade in the highlighting background in the MAC address beneath the PCs
      get('mac_address_pc_1_background').appendChild(createFadeInAnimation(animationIdPrefix + 'fadeInPc1MacAddressBackground', newMacAddressesFadeInStart));
      get('mac_address_pc_2_background').appendChild(createFadeInAnimation(animationIdPrefix + 'fadeInPc2MacAddressBackground', newMacAddressesFadeInStart));

      get('pcs_source_mac_address_group').appendChild(createFadeInAnimation(animationIdPrefix + 'fadeInSourceMacAddress', newMacAddressesFadeInStart));
      const lastAnimation = createFadeInAnimation(animationIdPrefix + 'fadeInDestinationMacAddress', newMacAddressesFadeInStart);
      get('pcs_destination_mac_address_group').appendChild(lastAnimation);

      return lastAnimation;
  }

  function sendFrameFromPc1ToPc2AndBack(beginOfAnimation) {

    const frameForPc2 = cloneLargeEthernetFrameAndMakeItSmall('frame_from_pc_1_to_pc_2');
    frameForPc2.style.opacity = '0';
    const cablePc1ToSwitchId = 'cable_pc1_switch';
    // Add the frame as a sibling to the path representing the cable
    get(cablePc1ToSwitchId).parentNode.appendChild(frameForPc2)

    const animationIdPrefix = 'pc1_to_pc2_';

    frameForPc2.appendChild(newSvgElem('set', objToMap({'attributeName': 'opacity', 'to': '1', 'begin': beginOfAnimation})));

    // We have to go backwards via the path since they were drawn in Inkscape in the direction are intuitive for the traffic (including flooding by the switch) between the laptops
    const goToSwitch1 = newAnimMotionWithMpath(cablePc1ToSwitchId, objToMap({'dur': '2s', 'id': 'goFromPc1ToSwitch', 'begin': beginOfAnimation, 'keyPoints': '1;0', 'keyTimes': '0;1', 'calcMode': 'linear'}));
    const goToSwitch1Center = moveOnPaths(animationIdPrefix, ['anim_path_switch_to_port_2', 'anim_path_go_above_port_2'], 'goFromPc1ToSwitch.end', true);
    const switch1CenterAnimationEnd = animationIdPrefix + 'anim_path_go_above_port_2_reversed.end'
    const moveInsidePc2 = moveOnPaths(animationIdPrefix, ['anim_path_from_center_to_above_port_4', 'anim_path_go_to_port_4', 'cable_switch_1_to_switch_2', 'port_1_to_switch_2_center', 'switch_2_anim_path_go_above_port_3', 'switch_2_anim_path_go_to_port_3', 'cable_pc2_switch', 'go_inside_pc_2'], switch1CenterAnimationEnd);

    // Once the frame has moved from PC 1 to switch 1, we show the PC's MAC address in the MAC address table of switch 1
    get('switch_1_pc_1_mac_address').appendChild(createSetVisible('goFromPc1ToSwitch.end'));
    // Once the frame has moved from the PC via switch 1 to switch 2, we show the PC's MAC address in the MAC address table of switch 2
    get('switch_2_pc_1_mac_address').appendChild(createSetVisible(animationIdPrefix + 'cable_switch_1_to_switch_2_motion.end'));

    const disappearAtPc2 = createFadeOutAnimation(animationIdPrefix + 'disappearAtPc2', animationIdPrefix + 'go_inside_pc_2_motion.end');
    const reappearAtPc2 = createFadeInAnimation(animationIdPrefix + 'reappearAtPc2', `${disappearAtPc2.id}.end`);

    const lastAnimationOfSwappingMacAddresses = swapSourceAndDestinationAddressInEthernetFrame(get('pcs_source_mac_address_group'), get('pcs_destination_mac_address_group'), animationIdPrefix, `${disappearAtPc2.id}.end`);

    const goBackToSwitch1 = moveOnPaths(animationIdPrefix, ['go_inside_pc_2', 'cable_pc2_switch', 'switch_2_anim_path_go_to_port_3', 'switch_2_anim_path_go_above_port_3', 'port_1_to_switch_2_center', 'cable_switch_1_to_switch_2', 'anim_path_go_to_port_4', 'anim_path_from_center_to_above_port_4'], `${lastAnimationOfSwappingMacAddresses.id}.end`, true);

    const goBackToPc1 = moveOnPaths(animationIdPrefix, ['anim_path_go_above_port_2', 'anim_path_switch_to_port_2', 'cable_pc1_switch'], animationIdPrefix + 'anim_path_from_center_to_above_port_4_reversed.end');

    // Once the frame has moved from PC 2 back to the switch 2, we show the PC's MAC address in the switch's MAC address table
    get('switch_2_pc_2_mac_address').appendChild(createSetVisible(animationIdPrefix + 'cable_pc2_switch_reversed.end'));
    // Once the frame from PC 2 has moved from switch 2 back to switch 1, we show PC 2's MAC address in the MAC address table of switch 1
    get('switch_1_pc_2_mac_address').appendChild(createSetVisible(animationIdPrefix + 'cable_switch_1_to_switch_2_reversed.end'));

    // The return frame from Pc 2 has arrived at Pc 1
    const disappearAtPc1 = createFadeOutAnimation(animationIdPrefix + 'disappearAtPc1', animationIdPrefix + 'cable_pc1_switch_motion.end');

    const allAnimationsOfFrameBetweenPcs = [goToSwitch1, goToSwitch1Center, moveInsidePc2, disappearAtPc2, reappearAtPc2, goBackToSwitch1, goBackToPc1, disappearAtPc1].flat();

    allAnimationsOfFrameBetweenPcs.forEach(animation => frameForPc2.appendChild(animation));


    // At switch 1, flood the frame also to laptop 1 since the port of destination host d1:d1 is unknown
    function addAnimationsOfFrameForLaptop1(){
      const frameForLaptop1 = cloneLargeEthernetFrameAndMakeItSmall('frame_from_switch_1_to_laptop_1');
      frameForLaptop1.style.opacity = '0';
      get(cablePc1ToSwitchId).parentNode.appendChild(frameForLaptop1);

      const showFrameForLaptop1 = createFadeInAnimation('showFrameForLaptop1', switch1CenterAnimationEnd);
      const movementsOfFrameForLaptop1 = moveOnPaths(animationIdPrefix, ['anim_path_port_1_to_switch_center', 'cable_laptop1_switch'], switch1CenterAnimationEnd, true)
      const disappearAtLaptop1 = createFadeOutAnimation('disappearAtLaptop1AfterFlooding', animationIdPrefix + 'cable_laptop1_switch_reversed.end');
      [showFrameForLaptop1, movementsOfFrameForLaptop1, disappearAtLaptop1].flat().forEach(movement => frameForLaptop1.appendChild(movement));
    }

    addAnimationsOfFrameForLaptop1();

    get('flooding_group_switch_1').appendChild(createSetVisible(switch1CenterAnimationEnd));
    get('flooding_group_switch_1').appendChild(createSetHidden('disappearAtLaptop1AfterFlooding.begin'));

    const switch2CenterAnimationEnd = animationIdPrefix + 'port_1_to_switch_2_center_motion.end';

    function addAnimationsOfFrameForLaptop2(){
      const frameForLaptop2 = cloneLargeEthernetFrameAndMakeItSmall('frame_from_switch_2_to_laptop_2');
      frameForLaptop2.style.opacity = '0';
      get(cablePc1ToSwitchId).parentNode.appendChild(frameForLaptop2);

      const showFrameForLaptop2 = createFadeInAnimation('showFrameForLaptop2', switch2CenterAnimationEnd);
      const movementsOfFrameForLaptop2 = moveOnPaths(animationIdPrefix, ['switch_2_anim_path_go_above_port_2', 'switch_2_anim_path_go_to_port_2', 'cable_laptop2_switch'], switch2CenterAnimationEnd)
      const disappearAtLaptop2 = createFadeOutAnimation('disappearAtLaptop2AfterFlooding', animationIdPrefix + 'cable_laptop2_switch_motion.end');
      [showFrameForLaptop2, movementsOfFrameForLaptop2, disappearAtLaptop2].flat().forEach(movement => frameForLaptop2.appendChild(movement));
    }
    addAnimationsOfFrameForLaptop2();

    get('flooding_group_switch_2').appendChild(createSetVisible(switch2CenterAnimationEnd));
    get('flooding_group_switch_2').appendChild(createSetHidden('disappearAtLaptop2AfterFlooding.begin'));

    // Return the last animation
    return allAnimationsOfFrameBetweenPcs.at(-1);

  }
  });
}
