* When listing the different attributes of the network (first IP, broadcast, subnet mask), make them clickable to explain how we got to that result, using bits probably.
* Show the extent of the network prefix using something interactive, like a slider. This will update the CIDR postfix after the IP address → Note form 2024-04-06: The slider might not be a good fit but we could make the bits clickable, like [here](https://evanw.github.io/float-toy/).
* Maybe have a box that allows to toggle these GUI components:
  * decimal values of bits, maybe the bits separately as well
  * the CIDR postfix → implemented in August 2024

* We can show how knowing the network part and the host part is useful:
 * Knowing if a host is within the local network or outside. In the latter case we need to contact the default gateway. Use two IP addresses (source and destination), show them in binary, and check if their network part matches, similar [this](https://youtu.be/_PuyU1PJDo0?list=PLIFyRwBY_4bQUE4IB5c4VPRyDoLgOdExE&t=515)
  * Range of usable host addresses
  * Network and directed broadcast addresses
* Finally we show how the subnet mask and the CIDR suffix are related
