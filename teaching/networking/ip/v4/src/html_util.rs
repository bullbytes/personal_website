use std::collections::HashMap;

use web_sys::{console, window, Document, Element, Event, HtmlInputElement, MessageEvent};

use wasm_bindgen::{closure::IntoWasmClosure, prelude::Closure, JsCast};

use crate::page_access::ElementId;

pub fn get_input_elem(elem_id: ElementId) -> HtmlInputElement {
    get_as(elem_id)
}

/// Gets an element by its `elem_id` and casts it to `T`.
pub fn get_as<T: JsCast>(elem_id: ElementId) -> T {
    let elem = get_elem(elem_id)
        .unwrap_or_else(|| panic!("Expected element with ID '{elem_id}' to exist"));

    elem.dyn_into().unwrap_or_else(|_| {
        let name_of_type = std::any::type_name::<T>();
        panic!("Expected element with ID '{elem_id}' to be of type '{name_of_type}'")
    })
}

/// Creates a new table data element.
pub fn new_td(text_content: &str, attributes: HashMap<&str, &str>) -> Element {
    let table_data = get_document().create_element("td").unwrap();
    table_data.set_text_content(Some(text_content));
    for (key, value) in attributes {
        let _ = table_data.set_attribute(key, value);
    }
    table_data
}

/// Creates a new `HtmlInputElement` with the given `attributes`.
pub fn new_input(attributes: HashMap<&str, &str>) -> HtmlInputElement {
    let input = get_document().create_element("input").unwrap();
    for (key, value) in attributes {
        let _ = input.set_attribute(key, value);
    }
    input.dyn_into::<HtmlInputElement>().unwrap()
}

/// Logs a string to the browser's console.
pub fn log_to_browser(log_msg: String) {
    console::log_1(&log_msg.into());
}

/// Adds or remove a `class` from an `element` based on `add_or_remove`.
pub fn add_or_remove_class(add_or_remove: bool, class: &str, element: &Element) {
    if add_or_remove {
        add_class(class, element);
    } else {
        remove_class(class, element);
    }
}

/// Adds a `class` to the `element` if it doesn't already have that `class`.
pub fn add_class(class: &str, element: &Element) {
    let res = element.class_list().add_1(class);
    match res {
        Ok(()) => {}
        Err(e) => {
            let elem_id = element.id();
            log_to_browser(format!(
                "Could not add class with name '{class}' to element '{elem_id}'. Error: {e:?}"
            ));
        }
    };
}
/// Removes a `class` from an `element`. Does nothing if that `element` doesn't have that `class`.
pub fn remove_class(class: &str, element: &Element) {
    let res = element.class_list().remove_1(class);
    match res {
        Ok(()) => {}
        Err(e) => {
            let elem_id = element.id();
            log_to_browser(format!(
                "Could not remove class with name '{class}' from element '{elem_id}'. Error: {e:?}"
            ));
        }
    };
}

/// Gets the first element that matches the `selector` inside the `parent_elem`.
pub fn query_selector(parent_elem: &Element, selector: &str) -> Option<Element> {
    match parent_elem.query_selector(selector) {
        Ok(found_elem_maybe) => found_elem_maybe,
        Err(err) => {
            let elem_id = parent_elem.id();
            log_to_browser(format!(
                    "Couldn't get element with selector '{selector}'. Search started at element with ID '{elem_id}'. Error is: {err:?}"
                ));
            None
        }
    }
}
/// Calls `query_selector_all` on `parent_elem` with the given `selector` and returns the result as a vector of `Element`s.
/// If the query fails, logs an error message.
/// See also JavaScript's
/// [querySelectorAll](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll).
pub fn query_selector_all(parent_elem: &Element, selector: &str) -> Vec<Element> {
    let mut result_vec = Vec::new();
    let query_result = parent_elem.query_selector_all(selector);
    match query_result {
        Ok(node_list) => {
            let mut i = 0;
            while let Some(node) = node_list.item(i) {
                let elem = node.dyn_into::<Element>().unwrap();
                i += 1;
                result_vec.push(elem);
            }
        }
        Err(error) => {
            let parent_elem_id = parent_elem.id();
            log_to_browser(format!("Query with selector '{selector}' called on '{parent_elem_id}' did not succeed. Error: {error:?}"));
        }
    }
    result_vec
}

/// Gets an [`Element`] by its `elem_id`.
/// Warns if the element doesn't exist.
pub fn get_elem(elem_id: ElementId) -> Option<Element> {
    let elem_maybe = get_document().get_element_by_id(&elem_id.to_string());
    if elem_maybe.is_none() {
        log_to_browser(format!("Couldn't get element with ID '{elem_id}'"));
    }

    elem_maybe
}

/// Gets an [`Element`] by its `elem_id`. If unsuccessful, panics with an error message.
pub fn get_elem_or_panic(elem_id: ElementId) -> Element {
    get_elem(elem_id).unwrap_or_else(|| panic!("Expected element '{elem_id}' to exist"))
}

/// Gets the document object.
pub fn get_document() -> Document {
    window()
        .expect("No global window exists")
        .document()
        .expect("There's no document in the window object")
}

/// Calls the `callback` once the `input_elem`'s text changes.
pub fn on_text_change<F>(input_elem: &HtmlInputElement, callback: F)
where
    F: Fn(&str) + 'static,
{
    set_oninput(input_elem, move |event: MessageEvent| {
        if let Some(target) = event.target() {
            let target_as_input_elem = target.dyn_ref::<HtmlInputElement>().unwrap();
            callback(&target_as_input_elem.value());
        } else {
            log_to_browser("Event target is none".to_string());
        }
    });
}

/// Sets the `oninput` field of `input_elem` to `callback`.
fn set_oninput<F>(input_elem: &HtmlInputElement, callback: F)
where
    F: IntoWasmClosure<dyn Fn(MessageEvent)> + 'static,
{
    // https://stackoverflow.com/questions/60054963/how-to-convert-closure-to-js-sysfunction
    let closure = Closure::new(callback);
    input_elem.set_oninput(Some(closure.as_ref().unchecked_ref()));
    closure.forget();
}

pub fn add_event_listener<F>(elem: &Element, event_name: &str, callback: F)
where
    F: IntoWasmClosure<dyn Fn(Event)> + 'static,
{
    let closure = Closure::new(callback);
    elem.add_event_listener_with_callback(event_name, closure.as_ref().unchecked_ref())
        .unwrap();
    closure.forget();
}

/// Gets the target of the `event` as a `T`.
/// Useful for event listeners where the target is known to be of type `T`.
pub fn get_target<T: JsCast>(event: &Event) -> T {
    let target = event.target().unwrap();
    target.dyn_into().unwrap()
}
