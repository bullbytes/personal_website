pub mod classes {
    pub const BYTE: &str = "byte";
    pub const BYTE_INPUT: &str = "byteInput";
    pub const BIT_WITH_INDEX: &str = "bitWithIndex";
    pub const CIDR_SUFFIX: &str = "cidrSuffix";
    pub const CIDR_SLASH: &str = "cidrSlash";
    pub const PARSE_WARNING_SYMBOL: &str = "parseWarningSymbol";
    pub const HIDDEN: &str = "hidden";
    pub const NETWORK_PART: &str = "networkPart";
    pub const HOST_PART: &str = "hostPart";
    pub const BOTH_NETWORK_AND_HOST_PART: &str = "bothNetworkAndHostPart";
    pub const ARROW: &str = "arrow";
    pub const ZERO_VALUE: &str = "zeroValue";
    pub const FILLER: &str = "filler";
    pub const SEPARATING_DOT: &str = "separatingDot";
    pub const HIGHLIGHTED_ROW: &str = "highlighted";
    pub const NO_WIDTH: &str = "noWidth";
    pub const HAS_BACKGROUND_AND_WIDTH_TRANSITION: &str = "hasBackgroundAndWidthTransition";
}
