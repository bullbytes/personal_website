mod bit;
mod css;
mod html_util;
mod page_access;
mod subnetting;

use bit::Bit;
use css::classes::{
    ARROW, BOTH_NETWORK_AND_HOST_PART, BYTE, BYTE_INPUT, CIDR_SLASH, FILLER,
    HAS_BACKGROUND_AND_WIDTH_TRANSITION, HIDDEN, HIGHLIGHTED_ROW, HOST_PART, NETWORK_PART,
    NO_WIDTH, SEPARATING_DOT, ZERO_VALUE,
};
use html_util::{
    add_class, add_event_listener, add_or_remove_class, get_as, get_elem, get_input_elem,
    get_target, log_to_browser, new_input, new_td, on_text_change, query_selector, remove_class,
};

use std::collections::{HashMap, HashSet};

use html_util::get_elem_or_panic as get;

use page_access::{
    get_bit_elems_of_byte, get_byte_and_bit_classes, get_byte_and_bit_classes_selector,
    get_byte_inputs, get_cidr_parse_warn_symbol, get_parse_warning_symbol_for_byte, ElementId,
};
use std::sync::{LazyLock, Mutex};
use wasm_bindgen::prelude::{wasm_bindgen, JsValue};
use web_sys::{js_sys::Array, Element, HtmlInputElement};
type ParseWarning = String;

// Called when the Wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    // Show Rust errors and stack traces in the browser console
    console_error_panic_hook::set_once();

    add_ip_address_table_and_change_listeners();

    // Show the subnetting part only if it's enabled in the options
    let subnetting_checkbox: HtmlInputElement = get_as(ElementId::SubnettingCheckbox);
    let should_show_subnetting = subnetting_checkbox.checked();
    show_gui_for_subnetting(should_show_subnetting);

    // Enable or disable the subnetting part when the user clicks the checkbox
    add_event_listener(&subnetting_checkbox, "change", |event| {
        let checkbox: HtmlInputElement = get_target(&event);
        let should_show_subnetting = checkbox.checked();
        show_gui_for_subnetting(should_show_subnetting);
    });

    // Set the initial value of the subnet mask depending on the network prefix
    set_values_in_subnet_mask_table(get_network_prefix_or_parse_warning(
        &get_input_elem(ElementId::CidrSuffixInput).value(),
    )?);

    // Once the user has changed the CIDR suffix, update all the GUI parts that depend on the CIDR
    // suffix such as the network and host parts in the IP address table and the subnet mask table
    on_text_change(
        &get_input_elem(ElementId::CidrSuffixInput),
        |new_user_input_for_cidr_suffix| {
            update_gui_for_network_prefix(new_user_input_for_cidr_suffix);
        },
    );

    Ok(())
}

fn set_values_in_subnet_mask_table(network_length: u8) {
    let subnet_mask_bits: [Bit; 32] =
        subnetting::get_subnet_mask_from_network_length(network_length);

    // We set the table data as the new children of the row. If the row didn't have any
    // children before, this will append the table data elements.
    get(ElementId::BitsOfSubnetMask)
        .replace_children_with_node(&create_table_data_from_bits(subnet_mask_bits));

    get(ElementId::DecimalValuesOfSubnetMask)
        .replace_children_with_node(&create_decimal_values_of_bits());

    let decimal_values_of_bits = &get(ElementId::DecimalValuesOfSubnetMask);
    Bit::get_bytes(&subnet_mask_bits)
        .into_iter()
        .enumerate()
        .for_each(|(byte_index, byte)| {
            mark_unused_decimal_values_of_bits(byte, byte_index, decimal_values_of_bits);
        });
}

/// Adds the IP address table with the byte inputs and the IP's binary representation to the page. Also adds event listeners to the byte inputs which update the binary representation of the IP address.
fn add_ip_address_table_and_change_listeners() {
    let input_bytes_and_cidr_suffix_row = get(ElementId::InputBytesAndCidrSuffix);
    let _ = input_bytes_and_cidr_suffix_row.append_with_node(&create_input_for_bytes());
    let _ = input_bytes_and_cidr_suffix_row.append_with_node(&create_input_for_cidr_suffix());

    // Those rows signify the conversion from decimal to binary
    add_row_with_arrows();

    // At first, we show the bits of the IP address with dummy values
    let _ =
        get(ElementId::BitsOfIP).append_with_node(&create_table_data_from_bits([Bit::Zero; 32]));

    let _ =
        get(ElementId::DecimalValuesOfBitsInIp).append_with_node(&create_decimal_values_of_bits());

    // We keep track of which byte inputs are incorrect. If one of them is incorrect, we'll
    // display a message about which range a byte can have (0 to 255).
    // The lifetime of this variable needs to be static to be moved into to the closure used in `on_text_change`
    static INVALID_BYTE_INPUT_INDICES: LazyLock<Mutex<HashSet<usize>>> =
        LazyLock::new(|| Mutex::new(HashSet::new()));

    // Get each input element where the user can enter the decimal value of a byte in the IP address
    for (byte_index, byte_input_elem) in get_byte_inputs().iter().enumerate() {
        // Show the information for each byte after the page has loaded
        update_ip_address_in_binary_for_byte(&byte_input_elem.value(), byte_index);

        // Once the user has changed a byte's decimal value, update the bits and
        // show a warning if the input is not a byte
        on_text_change(byte_input_elem, move |new_user_input_for_byte| {
            let parse_warning_maybe =
                update_ip_address_in_binary_for_byte(new_user_input_for_byte, byte_index);
            // The user input for that byte is not between 0 and 255
            if parse_warning_maybe.is_some() {
                INVALID_BYTE_INPUT_INDICES
                    .lock()
                    .unwrap()
                    .insert(byte_index);
            } else {
                INVALID_BYTE_INPUT_INDICES
                    .lock()
                    .unwrap()
                    .remove(&(byte_index));
            }

            // Show the text with the valid range of a byte if there's at least one incorrect byte input
            let show_byte_parse_warning = !INVALID_BYTE_INPUT_INDICES.lock().unwrap().is_empty();
            display(show_byte_parse_warning, &get(ElementId::ByteParseWarning));
        });
    }
}

fn show_gui_for_subnetting(subnetting_enabled: bool) {
    show_subnetting_elements(subnetting_enabled);

    // The highlighted class makes every second row easier to distinguish by setting the
    // background. But when subnetting is enabled, the input bytes and the bits of the
    // IP address have already a background color depending on whether they are in the network or host part, so
    // we remove the highlighted class.
    [ElementId::InputBytesAndCidrSuffix, ElementId::BitsOfIP]
        .iter()
        .map(|elem_id| get(*elem_id))
        .for_each(|elem| {
            let rows_should_be_highlighted = !subnetting_enabled;
            add_or_remove_class(rows_should_be_highlighted, HIGHLIGHTED_ROW, &elem);
        });

    if subnetting_enabled {
        update_gui_for_network_prefix(&get_input_elem(ElementId::CidrSuffixInput).value());
    } else {
        // Remove the classes in the IP address table that show the network and host part. This
        // is necessary when the subnetting was enabled before and the user now disabled it.
        remove_network_and_host_part_classes_from_ip_address();
    }
}

fn remove_network_and_host_part_classes_from_ip_address() {
    fn remove_classes(elem: &Element) {
        remove_class(NETWORK_PART, elem);
        remove_class(HOST_PART, elem);
        remove_class(BOTH_NETWORK_AND_HOST_PART, elem);
    }

    get_byte_inputs()
        .iter()
        .enumerate()
        .for_each(|(byte_index, byte_input)| {
            remove_classes(byte_input);

            for bit_elem in get_bit_elems_of_byte(ElementId::BitsOfIP, byte_index) {
                remove_classes(&bit_elem);
            }
        });
}

/// Updates the GUI elements that depend on the network prefix (the user can change that via the CIDR suffix):
/// - The byte inputs are marked as being in the network or host part
/// - The bits of the IP address are marked as being in the network or host part
/// - The network and host part text is updated
/// - The subnet mask table is updated
fn update_gui_for_network_prefix(user_input_for_cidr_suffix: &str) {
    match get_network_prefix_or_parse_warning(user_input_for_cidr_suffix) {
        Ok(network_prefix) => {
            set_network_and_host_part(network_prefix);
            mark_byte_inputs_according_to_cidr_suffix(network_prefix);
            mark_bits_according_to_cidr_suffix(network_prefix);

            set_values_in_subnet_mask_table(network_prefix);

            display_cidr_parse_warning(false);
        }
        Err(parse_warning) => {
            display_cidr_parse_warning(true);

            log_to_browser(parse_warning);
        }
    }
}

fn show_subnetting_elements(should_show_subnetting: bool) {
    // When subnetting gets enabled the first time, we add CSS class that enables transitions.
    // If the elements would have those transitions from the beginning, the elements would briefly
    // be visible when loading the page.
    if should_show_subnetting {
        let table = get(ElementId::IpAddressWithCidrSuffixTable);
        add_class(HAS_BACKGROUND_AND_WIDTH_TRANSITION, &table);
    }

    // This header spans over the slash and the CIDR suffix cells. When we want to hide the subnetting
    // parts, we set the width of that header to zero. This way, the cells with the IP address table
    // take up the remaining space.
    let cidr_table_header = get(ElementId::CidrTableHeader);
    html_util::add_or_remove_class(!should_show_subnetting, NO_WIDTH, &cidr_table_header);

    // Set .hidden on the networkAndHostPartText to avoid shifting of the elements below it
    let network_and_host_part_text = get(ElementId::NetworkAndHostPartText);
    display(should_show_subnetting, &network_and_host_part_text);
}

fn display_cidr_parse_warning(should_display: bool) {
    if let Some(cidr_parse_warning_symbol) = get_cidr_parse_warn_symbol() {
        display(should_display, &cidr_parse_warning_symbol);
    }

    if let Some(cidr_parse_warning) = get_elem(ElementId::CidrParseWarning) {
        display(should_display, &cidr_parse_warning);
    }
}

/// Adds or removes the class `hidden` from the `elem` depending on the value of `should_display`.
fn display(should_display: bool, elem: &Element) {
    add_or_remove_class(!should_display, HIDDEN, elem);
}

fn get_network_prefix_or_parse_warning(
    user_input_for_cidr_suffix: &str,
) -> Result<u8, ParseWarning> {
    let byte_maybe = user_input_for_cidr_suffix.parse::<u8>();
    match byte_maybe {
        Ok(byte) => match byte {
            0..=32 => Ok(byte),
            _ => Err(format!(
                "The CIDR suffix must be between 0 and 32, but was {byte}"
            )),
        },
        Err(err) => Err(format!(
            "Parse error for CIDR input '{user_input_for_cidr_suffix}': {err}"
        )),
    }
}

fn mark_bits_according_to_cidr_suffix(cidr_suffix: u8) {
    for byte_index in 0..=3 {
        for (bit_index, bit_elem) in get_bit_elems_of_byte(ElementId::BitsOfIP, byte_index)
            .iter()
            .enumerate()
        {
            let total_bit_index = byte_index * 8 + bit_index;

            let bit_is_in_network_part = (total_bit_index as u8) < cidr_suffix;
            let class_name = if bit_is_in_network_part {
                NETWORK_PART
            } else {
                HOST_PART
            };
            set_class_for_network_or_host_part(class_name, bit_elem);
        }
    }
}

fn mark_byte_inputs_according_to_cidr_suffix(cidr_suffix: u8) {
    let inputs_for_bytes = get_byte_inputs();

    let nr_of_bytes = inputs_for_bytes.len() as u8;
    // The fraction of how many bits are in the network part. Examples:
    // CIDR suffix is 24 → fraction is 0.75
    // CIDR suffix is 8 → fraction is 0.25
    // CIDR suffix is 15 → fraction is 0.46875
    let fraction_bits = f64::from(cidr_suffix) / 32.0;

    // From the fraction of bits in the network part, we calculate how many bytes are in the network part.
    // This is a float, for example 2.25 means that the first two bytes are completely in the network part and the third byte is partially in the network part.
    let number_of_bytes_in_network_part_float = fraction_bits * f64::from(nr_of_bytes);

    let number_of_bytes_completely_in_network_part = number_of_bytes_in_network_part_float as u8;

    for i in 0..number_of_bytes_completely_in_network_part {
        set_class_for_network_or_host_part("networkPart", &inputs_for_bytes[i as usize]);
    }
    for i in number_of_bytes_completely_in_network_part..nr_of_bytes {
        set_class_for_network_or_host_part("hostPart", &inputs_for_bytes[i as usize]);
    }

    // If the CIDR suffix is not a multiple of 8, there is one byte that's both part of the network and host parts → Set its class accordingly
    if number_of_bytes_in_network_part_float > number_of_bytes_completely_in_network_part.into() {
        set_class_for_network_or_host_part(
            "bothNetworkAndHostPart",
            &inputs_for_bytes[number_of_bytes_completely_in_network_part as usize],
        );
    }
}

/// Sets the class name of the `element` to `class_name` (which is the class name for belonging to the host part, network part, or both parts) and removes the other two classes.
fn set_class_for_network_or_host_part(class_name: &str, element: &Element) {
    match class_name {
        NETWORK_PART => {
            remove_class(BOTH_NETWORK_AND_HOST_PART, element);
            remove_class(HOST_PART, element);
        }
        HOST_PART => {
            remove_class(BOTH_NETWORK_AND_HOST_PART, element);
            remove_class(NETWORK_PART, element);
        }
        BOTH_NETWORK_AND_HOST_PART => {
            remove_class(NETWORK_PART, element);
            remove_class(HOST_PART, element);
        }

        _ => log_to_browser(format!("Unknown class name: {class_name}")),
    }

    add_class(class_name, element);
}

fn create_input_for_cidr_suffix() -> Array {
    let table_data_for_cidr_input = Array::new();
    let slash_table_data = new_td("/", HashMap::from([("class", CIDR_SLASH)]));
    slash_table_data.set_id(&ElementId::CidrSlash.to_string());

    let table_data_for_input = new_td("", HashMap::from([("colspan", "4")]));
    table_data_for_input.set_id("tableDataForCidrInput");

    let init_value_for_input = "8";
    table_data_for_input.replace_children_with_node_1(&new_input(HashMap::from([
        ("id", "cidrSuffixInput"),
        ("type", "number"),
        ("min", "0"),
        ("max", "32"),
        ("placeholder", init_value_for_input),
        ("value", init_value_for_input),
    ])));

    table_data_for_cidr_input.push(&slash_table_data);
    table_data_for_cidr_input.push(&table_data_for_input);
    table_data_for_cidr_input
}

/// Creates the input fields where the user can enter the IP address byte by byte.
fn create_input_for_bytes() -> Array {
    let initial_byte_values = ["24", "65", "133", "5"];

    let inputs_in_table_data = Array::new();

    for (cur_byte_index, init_value) in initial_byte_values.iter().enumerate() {
        let table_data_wrapper = new_td("", HashMap::from([("colspan", "8")]));

        let input = new_input(HashMap::from([
            ("type", "number"),
            ("class", "byteInput"),
            ("id", &format!("{BYTE_INPUT}{cur_byte_index}")),
            ("min", "0"),
            ("max", "255"),
            ("placeholder", init_value),
            ("value", init_value),
        ]));

        table_data_wrapper.append_child(&input).unwrap();
        inputs_in_table_data.push(&table_data_wrapper);

        // Add a cell with a separating dot after each byte input, except for the last with index 3
        if cur_byte_index < 3 {
            inputs_in_table_data.push(&create_separating_dot_table_data());
        }
    }

    inputs_in_table_data
}

/// Creates an array of table data (`<td>`) elements that contain the bit values. The bits are
/// grouped into bytes by separating dots, from by `create_separating_dot_table_data()`.
fn create_table_data_from_bits(bits: [Bit; 32]) -> Array {
    let bits_as_table_data = Array::new();

    for (bit_idx, bit) in bits.iter().enumerate() {
        let cur_byte_index = bit_idx / 8;

        // Inside a byte, the leftmost bit has index 7, rightmost bit has index 0
        let cur_relative_bit_index = 7 - bit_idx % 8;

        let bit_table_data = new_td(
            &bit.to_string(),
            HashMap::from([(
                "class",
                get_byte_and_bit_classes(cur_byte_index, cur_relative_bit_index).as_str(),
            )]),
        );
        bits_as_table_data.push(&bit_table_data);

        // Add a separating dot after each byte, except for the last with index three.
        if cur_relative_bit_index == 0 && cur_byte_index < 3 {
            bits_as_table_data.push(&create_separating_dot_table_data());
        }
    }

    bits_as_table_data
}

fn update_ip_address_in_binary_for_byte(
    user_input_for_byte: &str,
    byte_index: usize,
) -> Option<ParseWarning> {
    match get_byte_or_parse_warning(user_input_for_byte) {
        Ok(byte) => {
            show_output_for_valid_byte(byte_index, byte);
            None
        }
        Err(parse_warning) => {
            show_warnings_for_invalid_user_input(byte_index);
            Some(parse_warning)
        }
    }
}

/// Changes the GUI elements to show warnings in case the user has entered text that is not a byte.
/// This function's counterpart is `show_output_for_valid_byte`.
fn show_warnings_for_invalid_user_input(byte_index: usize) {
    // Show a symbol above the byte with invalid input
    if let Some(parse_warning_symbol) = get_parse_warning_symbol_for_byte(byte_index) {
        display(true, &parse_warning_symbol);
    }

    for bit_elem in get_bit_elems_of_byte(ElementId::BitsOfIP, byte_index) {
        bit_elem.set_text_content(Some("?"));
    }

    // Since we don't know which bits should be set for the invalid byte,
    // we mark all the decimal values of the bits as zero
    for elem in get_bit_elems_of_byte(ElementId::DecimalValuesOfBitsInIp, byte_index) {
        add_class("zeroValue", &elem);
    }
}

/// Shows the information for a byte, like its bits and the decimal value of each bit.
/// This function's counterpart is `show_warnings_for_invalid_user_input`.
fn show_output_for_valid_byte(byte_index: usize, byte: u8) {
    hide_parse_warning(byte_index);

    let decimal_values_of_bits_table_row = &get(ElementId::DecimalValuesOfBitsInIp);
    mark_unused_decimal_values_of_bits(byte, byte_index, decimal_values_of_bits_table_row);

    let bits_of_ip_elem = get(ElementId::BitsOfIP);
    for (bit_elem, bit) in get_bit_elements_and_bits(byte_index, byte, &bits_of_ip_elem) {
        bit_elem.set_text_content(Some(&bit.to_string()));
    }
}

fn add_row_with_arrows() {
    let arrows_as_table_data = Array::new();
    for byte_index in 0..=3 {
        arrows_as_table_data.push(&new_td(
            "⇩",
            HashMap::from([
                ("colspan", "8"),
                ("class", &format!("{ARROW} {BYTE}{byte_index}")),
            ]),
        ));
        // Add a filler table cell after each arrow, except for the last with index three
        if byte_index < 3 {
            arrows_as_table_data.push(&create_filler_table_data());
        }
    }
    let _ = get(ElementId::ArrowRow).append_with_node(&arrows_as_table_data);
}

fn hide_parse_warning(byte_index: usize) {
    if let Some(parse_warning_symbol) = get_parse_warning_symbol_for_byte(byte_index) {
        display(false, &parse_warning_symbol);
    }
}

/// Gets the bit elements at `byte_index` of the `container_elem` and returns a vector of pairs of the bit element and the corresponding bit of the `byte`.
fn get_bit_elements_and_bits(
    byte_index: usize,
    byte: u8,
    container_elem: &Element,
) -> Vec<(Element, Bit)> {
    let mut result = Vec::new();
    let bits = Bit::get_bits_of_byte(byte);
    bits.iter().enumerate().for_each(|(bit_index, bit)| {
        // The leftmost bit in the web page has index 7, the rightmost bit has index 0
        let bit_index_right_to_left = 7 - bit_index;
        // Get the bit using the class name of its byte and the bit's index
        let selector_for_byte_and_bit =
            get_byte_and_bit_classes_selector(byte_index, bit_index_right_to_left);
        if let Some(bit_elem) = query_selector(container_elem, &selector_for_byte_and_bit) {
            result.push((bit_elem, *bit));
        }
    });
    result
}

/// Adds a class to those decimal values of the bits which are not used in the IPv4 address' `byte`.
/// For example, if the `byte` is 23, these decimal values are marked as unused: 128, 64, 32, and 8.
/// 16, 4, 2, 1 are not marked since they are used.
fn mark_unused_decimal_values_of_bits(
    byte: u8,
    byte_index: usize,
    decimal_values_of_bits_table_row: &Element,
) {
    for (bit_elem, bit) in
        get_bit_elements_and_bits(byte_index, byte, decimal_values_of_bits_table_row)
    {
        add_or_remove_class(bit == Bit::Zero, ZERO_VALUE, &bit_elem);
    }
}

/// Creates the decimal values of the bits ("128 64 32 ...") as an array of table data elements.
fn create_decimal_values_of_bits() -> Array {
    let decimal_values_as_table_data = Array::new();

    for cur_byte in 0..=3 {
        // Go through each bit of a byte, from index 7 to 0
        for cur_exponent in (0..=7).rev() {
            // Over the iterations: 128, 64, 32, 16, 8, 4, 2, 1
            let decimal_value = u32::pow(2, cur_exponent.try_into().unwrap());

            let decimal_value_of_bit_table_data = new_td(
                &decimal_value.to_string(),
                HashMap::from([(
                    "class",
                    get_byte_and_bit_classes(cur_byte, cur_exponent).as_str(),
                )]),
            );
            decimal_values_as_table_data.push(&decimal_value_of_bit_table_data);
        }

        // Add a filler table cell after each byte, except for the last with index
        // three
        if cur_byte < 3 {
            decimal_values_as_table_data.push(&create_filler_table_data());
        }
    }
    decimal_values_as_table_data
}

fn create_filler_table_data() -> Element {
    new_td(" ", HashMap::from([("class", FILLER)]))
}
fn create_separating_dot_table_data() -> Element {
    new_td(".", HashMap::from([("class", SEPARATING_DOT)]))
}

fn get_byte_or_parse_warning(string_to_parse: &str) -> Result<u8, ParseWarning> {
    let byte_maybe = string_to_parse.parse::<u8>();
    match byte_maybe {
        Ok(byte) => Ok(byte),
        Err(parse_int_error) => {
            log_to_browser(format!(
                "Parse error for byte input '{string_to_parse}': {parse_int_error}"
            ));
            Err(format!(
                "'{string_to_parse}' is not a byte. A byte ranges from 0 to 255"
            ))
        }
    }
}

/// Shows how many bits are in the network part and how many in the host part.
fn set_network_and_host_part(network_prefix: u8) {
    let network_part = get(ElementId::NetworkPart);
    let host_part = get(ElementId::HostPart);

    let bits_or_bit = if network_prefix == 1 { "bit" } else { "bits" };

    network_part.set_text_content(Some(&format!("{network_prefix} {bits_or_bit}")));

    let host_part_bits = 32 - network_prefix;
    let bits_or_bit = if host_part_bits == 1 { "bit" } else { "bits" };
    host_part.set_text_content(Some(&format!("{host_part_bits} {bits_or_bit}")));
}
