use crate::bit::Bit;

/// Gets the subnet mask as an array of bits from the network length.
pub fn get_subnet_mask_from_network_length(network_length: u8) -> [Bit; 32] {
    let mut subnet_mask_bits = [Bit::Zero; 32];

    for bit_idx in 0..network_length {
        subnet_mask_bits[bit_idx as usize] = Bit::One;
    }
    for bit_idx in network_length..subnet_mask_bits.len() as u8 {
        subnet_mask_bits[bit_idx as usize] = Bit::Zero;
    }
    subnet_mask_bits
}
