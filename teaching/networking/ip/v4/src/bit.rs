use core::fmt;

/// A binary digit, which can be either 0 or 1.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Bit {
    Zero,
    One,
}

impl fmt::Display for Bit {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let bit_as_str = match self {
            Bit::Zero => "0",
            Bit::One => "1",
        };
        write!(f, "{bit_as_str}")
    }
}

impl From<u8> for Bit {
    fn from(u: u8) -> Self {
        match u {
            0 => Bit::Zero,
            1 => Bit::One,
            _ => panic!("u must be 0 or 1"),
        }
    }
}

impl From<Bit> for u8 {
    fn from(bit: Bit) -> Self {
        match bit {
            Bit::Zero => 0,
            Bit::One => 1,
        }
    }
}

impl Bit {
    /// Converts an array of u8 to an array of Bits.
    /// For example, [1, 0, 1, 0, 1, 0, 1, 0] becomes `[Bit::One, Bit::Zero, Bit::One, Bit::Zero, Bit::One, Bit::Zero, Bit::One, Bit::Zero]`
    pub fn from_array_of_u8(arr: &[u8]) -> Vec<Bit> {
        let mut bits :Vec<Bit> = Vec::new();

        for elem in arr {
            bits.push((*elem).into());
        }
        bits
    }

    /// Gets the bits of a byte as an array, with the most significant bit at index 0 and the least significant bit at index 7.
    /// For example, if the byte is 130, the array will be `[1, 0, 0, 0, 0, 0, 1, 0]`
    pub fn get_bits_of_byte(byte: u8) -> [Bit; 8] {
        let mut bits = [Bit::Zero; 8];
        for i in 0..=7 {
            let shifted_byte = byte >> i;
            // Get the rightmost bit of the shifted byte (least significant bit)
            let cur_bit_as_u8 = shifted_byte & 1;
            // For the first iteration, the cur_bit is the least significant bit and therefore we place
            // that bit at index 7 of the array (rightmost bit)
            bits[7 - i] = if cur_bit_as_u8 == 0 {
                Bit::Zero
            } else {
                Bit::One
            };
        }
        bits
    }

    // Converts an array of `bits` to an array of u8.
    // If the number of `bits` is not a multiple of 8, the last byte will be right-padded with zeros: `assert_eq!(Bit::get_bytes(&[Bit::One, Bit::One]), vec![192]);`
    pub(crate) fn get_bytes(bits: &[Bit]) -> Vec<u8> {
        let mut bytes = Vec::new();
        let mut cur_byte = 0;

        for (bit_idx, bit) in bits.iter().enumerate() {
            let relative_bit_idx = 7 - bit_idx % 8;

            // If the current bit is 1, set the corresponding bit in the current byte to 1 using a
            // bitwise OR
            cur_byte |= (*bit as u8) << relative_bit_idx;

            // We've finished a full byte or we're at the last bit
            if relative_bit_idx == 0 || bit_idx == bits.len() - 1 {
                bytes.push(cur_byte);
                cur_byte = 0;
            }
        }
        bytes
    }
}

#[test]
fn test_get_bytes() {
    let bits = Bit::from_array_of_u8(&[1, 0, 1, 0, 1, 0, 1, 0]);
    assert_eq!(Bit::get_bytes(&bits), vec![0b1010_1010]);

    let bits = Bit::from_array_of_u8(&[0, 0, 0, 0, 0, 0, 0, 0, 1]);
    assert_eq!(Bit::get_bytes(&bits), vec![0, 128]);

    assert_eq!(Bit::get_bytes(&[Bit::One, Bit::One]), vec![192]);
}

#[test]
fn test_get_bits_of_byte() {
    let byte = 0b1010_1010;
    let bits = Bit::get_bits_of_byte(byte);
    assert_eq!(bits.to_vec(), Bit::from_array_of_u8(&[1, 0, 1, 0, 1, 0, 1, 0]));

    let byte = 255;
    let bits = Bit::get_bits_of_byte(byte);
    assert_eq!(bits.to_vec(), Bit::from_array_of_u8(&[1, 1, 1, 1, 1, 1, 1, 1]));

    let byte = 3;
    let bits = Bit::get_bits_of_byte(byte);
    assert_eq!(bits.to_vec(), Bit::from_array_of_u8(&[0, 0, 0, 0, 0, 0, 1, 1]));

    let byte = 130;
    let bits = Bit::get_bits_of_byte(byte);
    assert_eq!(bits.to_vec(), Bit::from_array_of_u8(&[1, 0, 0, 0, 0, 0, 1, 0]));
}
