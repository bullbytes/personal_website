use std::fmt::{Display, Formatter};

use wasm_bindgen::JsCast;
use web_sys::{Element, HtmlInputElement};

use crate::html_util::{get_elem, log_to_browser, query_selector, query_selector_all};

use crate::css::classes::{BIT_WITH_INDEX, BYTE, CIDR_SUFFIX, PARSE_WARNING_SYMBOL};

/// Creates a class name for a bit.
/// Arguments:
/// - `byte_index`: The index of the byte the bit is in. First byte has index 0.
/// - `bit_index`: The index of the bit within the byte. The leftmost bit has index 7 and the
///                rightmost bit has index 0.
pub fn get_byte_and_bit_classes(byte_index: usize, bit_index: usize) -> String {
    format!("{BYTE}{byte_index} {BIT_WITH_INDEX}{bit_index}")
}

/// Gets the selector of a byte and a bit. Mind that within a byte the leftmost bit has index 7 and the rightmost bit has index 0.
pub fn get_byte_and_bit_classes_selector(byte_index: usize, bit_index: usize) -> String {
    // Get the bit using the class name of its byte and the bit's index
    // Mind the leading dot for the class name and that there must be no space between the
    // previous class name and the dot
    format!(".{BYTE}{byte_index}.{BIT_WITH_INDEX}{bit_index}")
}

pub fn get_cidr_parse_warn_symbol() -> Option<Element> {
    let selector = format!(".{CIDR_SUFFIX}.{PARSE_WARNING_SYMBOL}");
    get_elem(ElementId::SymbolsForParseWarnings)
        .and_then(|parent_elem| query_selector(&parent_elem, &selector))
}

/// Gets the four input elements where the user can enter the IP address.
pub fn get_byte_inputs() -> Vec<HtmlInputElement> {
    get_elems(vec![
        ElementId::FirstByteInput,
        ElementId::SecondByteInput,
        ElementId::ThirdByteInput,
        ElementId::FourthByteInput,
    ])
}

/// Gets the elements representing bits inside the `elem_id` from the byte with `byte_index`.
pub fn get_bit_elems_of_byte(elem_id: ElementId, byte_index: usize) -> Vec<Element> {
    if let Some(parent) = get_elem(elem_id) {
        query_selector_all(&parent, &format!(".{BYTE}{byte_index}"))
    } else {
        vec![]
    }
}
pub fn get_parse_warning_symbol_for_byte(byte_index: usize) -> Option<Element> {
    get_elem(ElementId::SymbolsForParseWarnings).and_then(|parent_elem| {
        query_selector(
            &parent_elem,
            &format!(".{PARSE_WARNING_SYMBOL}.{BYTE}{byte_index}"),
        )
    })
}

/// Gets the elements by their `ids`. Casts each element to `T`.
fn get_elems<T: JsCast>(ids: Vec<ElementId>) -> Vec<T> {
    let mut elems = Vec::new();
    for id in ids {
        if let Some(elem) = get_elem(id) {
            elems.push(elem.dyn_into().unwrap());
        } else {
            log_to_browser(format!("Element with ID {id} does not exist"));
        }
    }
    elems
}

#[derive(Debug, Clone, Copy)]
pub enum ElementId {
    DecimalValuesOfBitsInIp,
    BitsOfIP,
    InputBytesAndCidrSuffix,
    ArrowRow,
    FirstByteInput,
    SecondByteInput,
    ThirdByteInput,
    FourthByteInput,
    NetworkPart,
    HostPart,
    SymbolsForParseWarnings,
    ByteParseWarning,
    CidrParseWarning,
    SubnettingCheckbox,
    CidrSuffixInput,
    NetworkAndHostPartText,
    BitsOfSubnetMask,
    DecimalValuesOfSubnetMask,
    CidrSlash,
    CidrTableHeader,
    IpAddressWithCidrSuffixTable,
}
impl ElementId {
    fn as_string(&self) -> &str {
        match self {
            ElementId::DecimalValuesOfBitsInIp => "decimalValuesOfBitsInIp",
            ElementId::BitsOfIP => "bitsOfIP",
            ElementId::InputBytesAndCidrSuffix => "inputBytesAndCidrSuffix",
            ElementId::ArrowRow => "arrowRow",
            ElementId::FirstByteInput => "byteInput0",
            ElementId::SecondByteInput => "byteInput1",
            ElementId::ThirdByteInput => "byteInput2",
            ElementId::FourthByteInput => "byteInput3",
            ElementId::NetworkPart => "networkPart",
            ElementId::HostPart => "hostPart",
            ElementId::SymbolsForParseWarnings => "symbolsForParseWarnings",
            ElementId::ByteParseWarning => "byteParseWarning",
            ElementId::CidrParseWarning => "cidrParseWarning",
            ElementId::SubnettingCheckbox => "subnettingCheckbox",
            ElementId::CidrSuffixInput => "cidrSuffixInput",
            ElementId::NetworkAndHostPartText => "networkAndHostPartText",
            ElementId::BitsOfSubnetMask => "bitsOfSubnetMask",
            ElementId::DecimalValuesOfSubnetMask => "decimalValuesOfBitsInSubnetMask",
            ElementId::CidrSlash => "cidrSlash",
            ElementId::CidrTableHeader => "cidrTableHeader",
            ElementId::IpAddressWithCidrSuffixTable => "ipAddressWithCidrSuffixTable",
        }
    }
}

impl Display for ElementId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.as_string())
    }
}
