# IP Address Explorer
An interactive visualization of IP addresses. Work in progress.

## Build instructions

    # Just once
    cargo install wasm-pack
    # Build the application, including WebAssembly and JavaScript files
    wasm-pack build --target web

## Run instructions
Start a web server to serve the files `addresses.html`, `pkg/ip_explorer.js`, and `pkg/ip_explorer_bg.wasm`:

    # Just once
    cargo install https
    # Start the web server, listening to localhost:8000
    http

