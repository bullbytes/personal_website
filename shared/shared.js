function toggleVisibility(element) {
  const style = element.style;
  if (style.visibility === "hidden") {
    style.visibility = "visible";
  } else {
    style.visibility = "hidden";
  }
}

// Sets the details elements to show a different text when they are open or closed based on the data-open and data-close attributes of the details' summary element
function setUpDetailsElements() {

  // Get all elements of type "details"
  document.querySelectorAll("details").forEach(details => {

    const summary = details.querySelector("summary");
    const textWhenDetailsAreOpened = summary.getAttribute("data-open");
    const textWhenDetailsAreClosed = summary.getAttribute("data-close");

    summary.textContent = textWhenDetailsAreClosed;

    details.addEventListener("toggle", event => {
      /* The element was toggled open */
      if (details.open) {
        summary.textContent = textWhenDetailsAreOpened;
      }
      /* The element was toggled closed */
      else {
        summary.textContent = textWhenDetailsAreClosed;
      }
    });
  });
}
